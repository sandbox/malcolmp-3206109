<?php
/**
* @file
* Paypal API.
*/

/**
 * Payment Class.
 */

class PayPalPayment {
  protected $paypal_email='none';
  protected $sandbox=TRUE;
  protected $oid=0;
  protected $num_items=0;
  protected $data=[];

  public function __construct($oid) {
    $this->oid = $oid;
  }

  public function sandbox($sandbox) {
    $this->sandbox = $sandbox;
  }

  public function email($paypal_email) {
    $this->paypal_email = $paypal_email;
  }

  public function url() {
    // Paypal url
    if( $this->sandbox ) {
      $url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
    } else {
      $url = 'https://www.paypal.com/cgi-bin/webscr';
    }
    return $url;
  }

  public function isValid() {
    if(! filter_var($this->paypal_email, FILTER_VALIDATE_EMAIL) ) {
      return FALSE;
    }
    if(! isset($this->data['amount_1']) ) {
      return FALSE;
    }
  }

  private function notifyUrl() {
    $oid = $this->oid;
    $url = url("paypal_api/ipn/$oid", ['absolute' => TRUE,]);
    return $url;
  }

  private function cancelUrl() {
    $oid = $this->oid;
    $url = url("paypal_api/cancel/$oid", ['absolute' => TRUE,]);
    return $url;
  }

  private function returnUrl() {
    $oid = $this->oid;
    $url = url("paypal_api/return/$oid", ['absolute' => TRUE,]);
    return $url;
  }

  public function getData() {
    return $this->data;
  }

  public function asString() {
    $string = 'PayPalPayment: ';
    $string .= print_r($this->data, TRUE);
    return $string;
  }

  public function setData($items) {
    $this->num_items = count($items);
    global $language;
    $this->data = array(
      'cmd' => '_cart',
      'upload' => 1,
//    'display' => 1,
      'business' => $this->paypal_email,
      'notify_url' => $this->notifyUrl(),
      'charset' => 'utf-8',
      'no_note' => 1,
      'no_shipping' => 1,
      'cancel_return' => $this->cancelUrl(),
      'return' => $this->returnUrl(),
      'rm' => 2,                              // Return Method POST in variables
//    'paymentaction' => $controller_data['capture'], can be order or sale
      'currency_code' => 'GBP',
      'lc' => $language->language,
      'invoice' => $this->oid,
    );
    // Add each line item separately. The first item starts at 1.
    $index = 1;
    foreach ($items as $line_item) {
      $this->data['amount_' . $index] = $line_item['price'];
      $this->data['item_name_' . $index] = $line_item['name'];
      $this->data['quantity_' . $index] = $line_item['quantity'];
      $index++;
    }
  }
}

/**
 * Implements hook_menu().
 */
function paypal_api_menu() {

  // Paypal IPN
  $items['paypal_api/ipn/%'] = array(
    'title' => 'Paypal IPN',
    'page callback' => 'paypal_api_ipn',
    'file' => 'paypal_api_ipn.inc',
    'page arguments' => array(2),
    'access callback' => TRUE,
    'type' => MENU_NORMAL_ITEM,
  );

  // Paypal Cancel URL
  $items['paypal_api/cancel/%'] = array(
    'title' => 'Paypal IPN',
    'page callback' => 'paypal_api_cancel',
    'page arguments' => array(2),
    'access callback' => TRUE,
    'type' => MENU_NORMAL_ITEM,
  );

  // Paypal Resturn URL
  $items['paypal_api/return/%'] = array(
    'title' => 'Paypal IPN',
    'page callback' => 'paypal_api_return',
    'file' => 'paypal_api_ipn.inc',
    'page arguments' => array(2),
    'access callback' => TRUE,
    'type' => MENU_NORMAL_ITEM,
  );

  // Paypal Test Page
  $items['paypal_api/test'] = array(
    'title' => 'Paypal Test',
    'page callback' => 'paypal_api_test_page',
    'access callback' => TRUE,
    'type' => MENU_NORMAL_ITEM,
    'file' => 'paypal_api_test.inc',
  );

  // Paypal Pay
  $items['paypal_api/pay/%'] = array(
    'title' => 'Go to payment server',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('paypal_api_form_pay', 2),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  return $items;
}

function paypal_api_return($oid) {
  drupal_set_message("Return $oid");
  drupal_goto("<front>");
}

function paypal_api_cancel($oid) {
  drupal_set_message("Cancel $oid");
  drupal_goto("<front>");
}

function paypal_api_form_pay(array $form, array &$form_state, $oid) {
  watchdog('paypal_api', "Paying for order $oid");
  $payment = new PayPalPayment($oid);
  // If zero or not an integer its a test payment
  $number = (preg_match("/^\d+$/", $oid));
  if(!$number || intval($oid) == 0 ) {
    $data = [
      'sandbox' => 1,
      'paypal_email' => 'none',
      'price' => '2.00',
    ];
    $data = variable_get('paypal_api_data', $data);
    $paypal_email = $data['paypal_email'];
    watchdog('paypal_api', "Test payment $oid paypal_email $paypal_email");
    if( $data['paypal_email'] == 'none' ) {
      $valid = FALSE;
    } else {
      $valid = TRUE;
      $payment->email($data['paypal_email']);
      $payment->sandbox($data['sandbox']);
      $item = ['price' => $data['price'], 'name' =>'PayPal API Test', 'quantity' =>1 ];
      $payment->setData([$item]);
    }
  } else {
    // invoke hook to populate payment from the $oid.
    // if we don't get anything then abort the payment
    $payment = module_invoke_all('paypal_api_payment', $payment);
    $valid = $payment->isValid();
  }
  // Return an error form;
  if( !$valid ) {
    $form['message'] = array(
      '#type' => 'markup',
      '#markup' => "<h3>ERROR: payment not valid</h3>".$payment->asString(),
    );
    return $form;
  }
  watchdog('paypal_api', "Valid payment, " . $payment->asString() );

  $url = $payment->url();
  // Build the form.
  $form['#action'] = url($url, array( 'external' => TRUE,));

  $data = $payment->getData();
  foreach ($data as $name => $value) {
    if (!empty($value)) {
      $form[$name] = array(
        '#type' => 'hidden',
        '#value' => $value,
      );
    }
  }
  $form['message'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t('Please wait. You will be redirected to PayPal to authorize the payment. No details are help by congress.org.uk') . '</p>',
  );
  // We need form submission as quickly as possible, so use light inline code.
  $form['js'] = array(
    '#type' => 'markup',
    '#markup' => '<script type="text/javascript">document.getElementById(\'paypal-api-form-pay\').submit();</script>',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
  );

  return $form;
}
