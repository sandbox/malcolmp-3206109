<?php
// $Id$
/**
 * @file
 * Functions for league fixture tab
 */

const PAYPAL_IPN_SERVER_URL = 'https://www.paypal.com/cgi-bin/webscr';
const PAYPAL_IPN_SANDBOX_SERVER_URL = 'https://www.sandbox.paypal.com/cgi-bin/webscr';


/**
 * process Paypal IPN
 */
function paypal_api_ipn($oid) {
  watchdog('paypal_api', "Got IPN for $oid");
  // Print out POST variables
  $ipn_variables = $_POST;
  foreach ($_POST as $ipn_variable => $value) {
    $ipn_variables[$ipn_variable] = rawurldecode($value);
  }
  watchdog('paypal_api', "IPN Variables: " . print_r($ipn_variables, TRUE));
  // Make a call back to paypal to validate the IPN
  $ok = paypal_api_acknowledge($ipn_variables);
  if( $ok ) {
    watchdog('paypal_api', "IPN is valid: ");
  }

  // Validate the invoice
  if($invoice = paypal_api_validate_variable($ipn_variables, 'invoice') ) {
    if( $invoice != $oid ) {
      watchdog('paypal_api', "IPN invoice $invoice not equal to order $oid");
      return;
    }
  } else {
    return FALSE;
  }
  // Get the paypal transaction number
  $txn = paypal_api_validate_variable($ipn_variables, 'txn_id');
  if( !$txn) {
    return FALSE;
  }
  if( intval($oid) == 0 ) {
    watchdog('paypal_api', "Test IPN OrderID $oid transaction $txn");
  } else {
    module_invoke_all('paypal_api_ipn', $oid, $txn, $ipn_variables);
  }
}

function paypal_api_validate_variable(array $ipn_variables, $variable) {
  if( isset($ipn_variables[$variable]) ) {
    $value = $ipn_variables[$variable];
  } else {
    watchdog('paypal_api', "IPN variable $variable not present");
    $value = FALSE;
  }
  return $value;
} 

// Got back to paypal to get the data
function paypal_api_acknowledge(array $ipn_variables) {
  // Prepare the request data.
  $ipn_variables['cmd'] = '_notify-validate';
  $data = [];
  foreach ($ipn_variables as $variable => $value) {
    $data[] = $variable . '=' . rawurlencode($value);
  }
  $data = implode('&', $data);

  // Execute the request.
  $url = empty($ipn_variables['test_ipn']) ? PAYPAL_IPN_SERVER_URL : PAYPAL_IPN_SANDBOX_SERVER_URL;
  // Use url() so we can alter the request using hook_url_outbound_alter().
  $url = url($url, array(
    'external' => TRUE,
  ));
  // Post using curl
  $curl = curl_init();
  curl_setopt($curl, CURLOPT_POST, 1);
//curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, TRUE);
  curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
//$data_string = http_build_query($data);
//curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
  $length = 0;
  $return = curl_exec($curl);
  curl_close ($curl);
//$response = chr_curl_http_request($url, array(
//    'method' => 'POST',
//    'data' => $data,
//    'curl_opts' => [
//      CURLOPT_SSL_VERIFYPEER => TRUE,
//      CURLOPT_SSL_VERIFYHOST => 2,
//    ],
//  ));
// Process the response.
  if(isset($return)) {
    $length = strlen($return);
  }
  watchdog('paypal_api', "Curl command completed with response size $length");
  if($length == 0) {
    return FALSE;
  }
  $response = print_r($return, TRUE);
  watchdog('paypal_api', "Paypal response: $response");
  return $return;
}


