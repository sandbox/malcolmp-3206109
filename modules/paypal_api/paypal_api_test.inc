<?php
// $Id$

// Club admin tab
function paypal_api_test_page() {
  $content['form'] = drupal_get_form('paypal_api_test_form');
  return $content;
}

/**
 *  Form for adding a new club.
 */
function paypal_api_test_form($form, &$form_state) {

  $form['paypal_email'] = [
    '#type' => 'textfield',
    '#size' => 30,
    '#maxlength' => 30,
    '#title' => t('Paypal Account EmailId'),
    '#required' => TRUE,
  ];

  // Pay button
  $form['submit'] = array(
    '#value' => t('Pay'),
    '#type'  => 'submit',
  );

  return $form;
}

function paypal_api_test_form_submit($form, &$form_state) {
  $paypal_email = $form_state['values']['paypal_email'];
  drupal_set_message("Paypal email $paypal_email");
  // Store the settings in a variable so the pay form can get them.
  $data = [
    'sandbox' => 1,
    'paypal_email' => $paypal_email,
    'price' => '2.00',
  ];
  variable_set('paypal_api_data', $data);
  // Create a unique order ID ('T' ensures it never a valid int)
  $orderID = 'T' . uniqid();
  // Then redirect to the pay form
  $form_state['redirect'] = "paypal_api/pay/$orderID";
}

