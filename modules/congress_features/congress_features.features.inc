<?php
/**
 * @file
 * congress_features.features.inc
 */

/**
 * Implements hook_node_info().
 */
function congress_features_node_info() {
  $items = array(
    'congress_report' => array(
      'name' => t('Congress Report'),
      'base' => 'node_content',
      'description' => t('Report about a previous congress. '),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
