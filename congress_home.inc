<?php
// $Id$
/**
 * @file
 * Functions for congress home tabs
 */

/**
 * display content of congress home tab
 */
function congress_home_page($congress) {
  $title = $congress->name;
  drupal_set_title(' ');
  $cid = $congress->cid;
  $rid = $congress->rid;
  if(congress_online_entry_open($congress)) {
    $wording = 'Enter online';
  } else {
    $wording = 'Online Entry is closed';
  }
  if($congress->status == 'I') {
    $wording = 'Inactive Congress DO NOT ENTER ' . $wording;
  }
  if($congress->status == 'H') {
    $wording = 'Hidden Congress ENTER TO TEST ONLY ' . $wording;
  }
  $text = l($wording,
            "congress/$cid/enter",
            array(
              'attributes' => array(
              'id' => 'congress-enter-link',
              'class' => 'congress-link'
              ),
            )
           );

  $last_date = congress_online_entry_last_date($congress);
  $info = '<table id="congress-enter-info">';
  $info .= "<tr><td>Last day to enter online</td><td>$last_date</td></tr>";
  if( $congress->late_fee >0) {
    $late_date = date('Y-m-d',$congress->late_date);
    $info .= '<tr><td>Entries after ' . $late_date . '</td>';
    $info .= '<td>£' . $congress->late_fee . ' late fee</td></tr>';
  }
  if( $congress->late_fee2 >0) {
    $late_date = date('Y-m-d H:i',$congress->late_date2);
    $info .= '<tr><td>Entries after ' . $late_date . '</td>';
    $info .= '<td>£' . $congress->late_fee2 . ' late fee</td></tr>';
  }
  
  if( $congress->pmid !=0 ) {
    // Check for test paypal
    $payment_method = entity_load_single('payment_method', $congress->pmid);
    $server = $payment_method->controller_data['server'];
    if (strpos($server, 'sandbox') !== false) {
      $info .= "<tr><td>Paypal Sending to</td><td>Test Server</td></tr>";
    }
  }

  // Check max entries
  if( $congress->max_entrants > 0 ) {
    $num_left = $congress->max_entrants - congress_entry_count($congress);
    if($num_left < 5) {
      if($num_left <= 0) {
        $num_left = "0 full - contact organiser";
      } else {
        $num_left = "less than 5";
      }
    }
    $info .= "<tr><td>Places Remaining</td><td>$num_left</td></tr>";
  }
  $info .= '</table>';
  $text .= $info;

  $text .= '<div id="congress-home-text">' . $congress->entry_text . '</div>';
  $output = array(
     '#type' => 'markup',
     '#markup' => "<div>$text</div>",
  );
  return $output;
}
