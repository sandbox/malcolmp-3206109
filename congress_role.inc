<?php
// $Id$
/**
 * @file
 * Functions for congress role
 */

/**
 * goto latest congress if there is one or display a message
 */
function congress_role_page($rid) {
  // Get most recent upcoming congress (if any)
  $cid = congress_next_for_role($rid);
  $role = congress_organisation_load($rid);
  $name = $role->name;
  if( $cid == 0 ) {
    $text = "Currently there is no $name congress taking entries"; 
  } else {
    drupal_goto("congress/$cid/home");
  }
  $output = array(
     '#type' => 'markup',
     '#markup' => "<h2>$text</h2>",
  );
  return $output;
}

function congress_next_for_role($rid) {
  $query = db_select('congress', 'c');
  $query->fields('c', array('cid'));
  $query->condition('rid', $rid);
  $query->condition('status', 'A');
  $lastWeek = time() - (7 * 24 * 60 * 60);
  $query->condition('date', $lastWeek, '>');
  $query->orderBy('c.date', 'ASC');
  $query->range(0, 1);
  $result = $query->execute();
  $row = $result->fetchAssoc();
  if(!$row) {
    $cid = 0;
  } else {
    $cid = $row['cid'];
  }
  return $cid;
}
