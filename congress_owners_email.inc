<?php
/**
* @file
* Functions for updating a congress.
*/

function congress_mass_mail_owners() {

  $title = "Congress Mass email for owners";
  $mail_limit = 1000;

  $content = user_tools_mass_email_page($title, 'Congress Organisation', 'congress', 'owners', $mail_limit );

  return $content;
}
