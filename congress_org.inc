<?php
// $Id$
/**
 * @file
 * Functions for congress org
 */

/**
 * display content of congress org page
 */
function congress_org_page($rid) {

  $content = array();

  if( count(congress_organisations_for_role($rid)) == 0 ) {
    $congress_org = entity_get_controller('congress_organisation')->create($rid);
    $title = "Create Congress Organisation";
  } else {
    $congress_org = congress_organisation_load($rid);
    $title = "Update Congress Organisation";
  }

  $content['title'] = array(
     '#type' => 'markup',
     '#markup' => "<h3>$title</h3>",
    );
 
  $content['form'] = drupal_get_form('congress_organisation_update_form', $congress_org);

  $link = l('Add a congress report', "node/add/congress-report/$rid");
  $content['addrep'] = array(
     '#type' => 'markup',
     '#markup' => "<div>$link</div>",
    );

  $table = congress_list_table($rid);
  $rows = $table['data'];     // Actual table data
  $header = $table['header']; // Headers
  $content['list']['#rows'] = $rows;
  $content['list']['#header'] = $header;
  $content['list']['#theme'] = 'table';

  $content['status'] = congress_data_status();
  $content['help'] = congress_rating_rules_help();

  return $content;
}

/**
*  Form for updating congress organisation details.
*/
function congress_organisation_update_form($form, &$form_state, $congress_org) {

  $form['#congress_org'] = $congress_org;
  $form_state['congress_org'] = $congress_org;

  if($congress_org->rid==0) {
    $btitle = 'Add Congress Organisation';
  } else {
    $btitle = 'Update Congress Organisation';
  }

  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
     '#value' => t($btitle),
     '#type'  => 'submit',
     '#weight' => 5,
  );

  $form['name'] = array(
   '#type'  => 'textfield',
   '#size'  => 25,
   '#title'  => t('Name of Congress Organisation'),
   '#default_value'  => $congress_org->name,
   '#maxlength'  => 30,
   '#weight' => 9,
  );

  $logos = congress_list_logos();
  $form['logo'] = array(
   '#title'  => t('Image File'),
   '#type'  => 'select',
   '#options' => $logos,
   '#default_value'  => $congress_org->logo,
   '#weight' => 12,
  );

  field_attach_form('congress_organisation', $congress_org, $form, $form_state);

  return $form;
}

/**
*  Action after the form is submitted.
*  Update congress record
*/
function congress_organisation_update_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $congress_org = $form_state['congress_org'];
  $congress_org->name = $form_state['values']['name'];
  $congress_org->logo = $form_state['values']['logo'];

  // Notify field widgets
  field_attach_submit('congress_organisation', $congress_org, $form, $form_state);

  // Save congress
  if( congress_organisation_save($congress_org) ) {
    drupal_set_message(t('Congress Organisation Saved'));
  } else {
    drupal_set_message(t('Error saving Congress Organisation'),'error');
  }
  drupal_goto('congress/admin/' . $congress_org->rid );
}

// Display org page from congress tab

function congress_org_from_congress($congress) {
  drupal_set_title(' ');
  $rid = $congress->rid;
  return congress_org_page($rid);
}
