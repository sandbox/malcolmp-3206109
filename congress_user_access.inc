<?php
/**
* @file
* Functions for updating user settings.
*/

function congress_user_access_tab($user) {

  $content = congress_user_organisations_list($user);

  return $content;
}

function congress_user_organisations_list($user) {
  $query = db_select('user_tools_owner', 'own');
  $query->condition('uid', $user->uid);
  $query->innerJoin('congress_organisation', 'co', 'co.rid = own.id');
  $query->addField('co', 'name', 'name');
  $query->addField('co', 'rid', 'id');
  $result = $query->execute();

  $items = array();
  foreach( $result as $row) {
    $link = l( $row->name, "/home/$row->id");
    $items[] = $link;
  }

  $content['list'] = array(
    '#items' => $items,
    '#theme' => 'item_list',
  );

  return $content;
}
