<?php
// $Id$
/**
 * @file
 * Functions for congress home tabs
 */

// Library for writing XL files
$exlib = libraries_get_path('vendor');
require_once "$exlib/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
 * congress reports in excel
 */
function congress_reports_xl($congress) {
  $cid = $congress->cid;

  // Create new PHPExcel object
  $objPHPExcel = new Spreadsheet();

  // Create a workbook
  $objPHPExcel->setActiveSheetIndex(0);

  $first = TRUE;
  foreach($congress->sections as $section) {
    if($first) {
      $sheet = $objPHPExcel->getActiveSheet();
      $first = FALSE;
    } else {
      $sheet = $objPHPExcel->createSheet();
    }
    $title = congress_string2filename($section->name);
    $sheet->setTitle($title);
    // Get section report
    $table = congress_entries_report($congress,$section);
    // Output to XL
    congress_xl_table($sheet, $table);
  }

  // send HTTP headers
  $fileName = 'Report.xls';
  drupal_add_http_header('Content-Type', 'application/vnd.ms-excel');
  drupal_add_http_header('Content-Disposition', 'attachment; filename="' . $fileName . '"');
  drupal_add_http_header('Cache-Control',  'max-age=0');
//$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
  $objWriter = new \PhpOffice\PhpSpreadsheet\Writer\Xls($objPHPExcel);
  $objWriter->save("php://output");
  exit;
}


/**
 * display content of congress reports tab
 */
function congress_reports_page($congress) {
  drupal_set_title(' ');
  $cid = $congress->cid;
  $link = l('Excel', "congress/$cid/reportsxl");
  $content = array();
  $content['link']['#markup'] = $link;
  $content['link']['#type'] = 'markup';
  foreach($congress->sections as $section) {
    // Get section entrants
    $table = congress_entries_report($congress,$section);
    $rows = $table['data'];     // Actual table data
    $header = $table['header']; // Headers

    // Account for totals rows
    $nrows = count($rows) - 1;
    $title = $section->name . ' (' . $nrows . ')';
    $content[$section->sid]['header'] = array(
     '#type' => 'markup',
     '#markup' => "<h2>$title</h2>",
    );
    $content[$section->sid]['list']['#rows'] = $rows;
    $content[$section->sid]['list']['#header'] = $header;
    $content[$section->sid]['list']['#theme'] = 'table';
  }
  return $content;
}

function congress_entries_report($congress,$section) {

  $cid = $congress->cid;
  $sid = $section->sid;
  $fnnum = 0;
  
  $header = array();
  $header[] = array('data' => 'First Name', 'field' => $fnnum++, 'char'=>TRUE );
  $header[] = array('data' => 'Last Name', 'field' => $fnnum++, 'char'=>'TRUE' );
  $header[] = array('data' => 'Club ',   'field' => $fnnum++, 'char'=>TRUE );
  $header[] = array('data' => 'Gross',   'field' => $fnnum++, 'char'=>FALSE );
  $header[] = array('data' => 'Paypal',   'field' => $fnnum++, 'char'=>FALSE );
  $header[] = array('data' => 'Net',   'field' => $fnnum++, 'char'=>FALSE );
  $header[] = array('data' => 'Status',   'field' => $fnnum++, 'char'=>TRUE );
  $header[] = array('data' => 'Membership',   'field' => $fnnum++, 'char'=>TRUE );
  if($congress->country == 'ENG') {
    $org = ecfdata_get_player_org();
  } else {
    $org = csdata_get_player_org();
  }

  $query = db_select('congress_entry', 'e');
  $query->fields('e', array('eid', 'first_name', 'last_name', 'price', 'silver', 'status', 'federation'));
  $query->condition('e.cid', $cid);
  $query->innerJoin('congress_entry_sections', 'ces', 'ces.eid = e.eid');
  $query->condition('ces.sid', $sid);
  $query->condition('e.status', congress_entry_visible_states(), 'IN');
  $query->leftJoin('rating_list_clubs', 'c', "c.club = e.club AND c.org=$org");
  $query->addField('c', 'name', 'club');
  $query->leftJoin('player_list_player', 'plp', "plp.pid = e.ecfcode AND plp.org=$org");
  $query->addField('plp', 'mdate', 'mdate');
  $query->addField('plp', 'status', 'cat');

  $norders = '(select count(*) from congress_entry where oid=e.oid and oid !=0)';
  $query->addExpression($norders, 'norders');
  // Make table sortable
//$query = $query->extend('TableSort')->orderByHeader($header);

//print out query
//drupal_set_message((string)$query);
  $result = $query->execute();

  $membership = congress_membership_needed($section, $congress);

  $dbrows = array();
  foreach ( $result as $row) {
    $eid = $row->eid;
    $dbrows[$eid]['first_name'] = $row->first_name;
    $dbrows[$eid]['last_name'] = $row->last_name;
    $dbrows[$eid]['club'] = $row->club;
    $dbrows[$eid]['price'] = $row->price;
    $dbrows[$eid]['norders'] = $row->norders;
    $dbrows[$eid]['status'] = $row->status;
    if( isset($row->cat) ) {
      $dbrows[$eid]['cat'] = $row->cat;
    } else {
      $dbrows[$eid]['cat'] = 'N';
    }
    $dbrows[$eid]['silver'] = $row->silver;
    $dbrows[$eid]['federation'] = $row->federation;
    if(isset($row->mdate) ) {
      $dbrows[$eid]['mdate'] = $row->mdate;
    } else {
      $dbrows[$eid]['mdate'] = 0;
    }
  }

  $total_gross = 0.0;
  $total_net = 0.0;
  $total_paypal = 0.0;
  $rows = array();
  foreach ( $dbrows as $eid => $dbrow) {
    $row=[];
    $row[] = $dbrow['first_name'];
    $row[] = $dbrow['last_name'];
    $row[] = $dbrow['club'];
    $price = 0.0;
    $paypal = 0.0;
    $net = 0.0;
    if($dbrow['status'] != 'ADMIN_FREE' && $dbrow['status'] != 'OTHER_PAID') {
      $price = (float)$dbrow['price'];
      // Paypal 2.9% + 30p per transaction
      if($dbrow['norders'] > 0) {
        $paypal = ($price * 0.029) + (0.3 / (float)$dbrow['norders']);
      }
      $net = $price - $paypal;
    }
    $row[] = number_format($price,2);
    $row[] = number_format($paypal,2);
    $row[] = number_format($net,2);
    $row[] = $dbrow['status'];
    $total_gross += round($price,2,PHP_ROUND_HALF_DOWN);
    $total_net += round($net,2,PHP_ROUND_HALF_DOWN);
    $total_paypal += round($paypal,2,PHP_ROUND_HALF_DOWN);
//  drupal_set_message('Name ' . $dbrow['last_name'] . " price $price total_gross $total_gross status " . $dbrow['status']);
    // See if they have the membership that they claimed if needed.
    if($congress->country == 'ENG') {
      $memberText = $dbrow['cat'] . ' ';
      if($membership == 'Silver') {
        if( ($dbrow['silver'] != 'N' && $dbrow['silver'] != 'M' && $dbrow['silver'] != 'B' && $dbrow['silver'] != 'J' && $dbrow['silver'] != 'E') && $dbrow['cat'] != 'G' && $dbrow['cat'] != 'S' && $dbrow['cat'] != 'P' && $dbrow['cat'] != 'K' ) {
          $memberText .= 'ERROR';
        }
      }
      if($membership == 'Gold') {
        if($dbrow['federation'] == 'ENG' ) {
          if( ($dbrow['silver'] == 'G' || $dbrow['silver'] == 'P' || $dbrow['silver'] == 'K' || $dbrow['silver'] == 'L') && $dbrow['cat'] != 'G' && $dbrow['cat'] != 'P' && $dbrow['cat'] != 'L' ) {
            $memberText .= 'ERROR';
          }
        }
      }
      if($dbrow['cat'] != 'N' && $dbrow['mdate'] < $congress->date) {
        $memberText .= ' EXPIRE';
      }
      $row[] = $memberText;
    } else {
      $memberText = $dbrow['cat'] . ' ';
      if($membership == 'SCO' && $dbrow['cat'] == 'N' && $congress->member_discount != 'N' && $dbrow['silver'] == 'Y' ) {
        $memberText .= 'ERROR';
      }
      $row[] = $memberText;
    }
    $rows[] = $row;
  } 

  // Sort

  congress_table_sort($rows, $header, 'Last Name');

  // Totals
  $totals = ['Total', ' ', ' ', number_format($total_gross,2), number_format($total_paypal,2), number_format($total_net,2), ' ', ' '];
  $rows[] = $totals;


  $table = array(
    'header' => $header,
    'data' => $rows,
  );

  return $table;
}

/**
 * Create table as Excel
 */
function congress_xl_table($worksheet, $table) {

  $headings = $table['header']; // Headers

  $row = 1;
  $cell = 1;
  foreach ($headings as $heading ) {
//  $worksheet->SetCellValueByColumnAndRow($cell, $row, $heading['data']);
    $worksheet->SetCellValue([$cell, $row], $heading['data']);
    $cell++;
  }

  $data = $table['data']; // Actual table data
  $row++;
  foreach ($data as $card ) {
    $cell = 1;
    foreach ($headings as $heading ) {
      if(isset($card[$heading['field']]) ) {
        $value = $card[$heading['field']];
      } else {
        $value = ' ';
      }
//    $worksheet->SetCellValueByColumnAndRow($cell, $row, $value);
      $worksheet->SetCellValue([$cell, $row], $value);
      $cell++;
    }
    $row++;
  }
}
