<?php
// $Id$
/**
 * @file
 * Functions for congress home tabs
 */

/**
 * display content of congress home tab
 */
function congress_entrants_page($congress) {
  $title = $congress->name;
  drupal_set_title(' ');
  $cid = $congress->cid;
  $content = array();
  $content['note'] = array(
   '#type' => 'markup',
   '#markup' => "<div>$congress->entrants_text</div>",
  );

  foreach($congress->sections as $section) {
    $sid = $section->sid;
    $table = congress_entrants_table($congress, $section);
    $header = $table['header'];          // Header
    $rows = $table['rows'];              // Actual table data
    $title = $table['title'] . ' (' . count($rows) . ')';
    $content[$sid]['title'] = array(
     '#type' => 'markup',
     '#markup' => "<h2>$title</h2>",
    );
    $content[$sid]['list']['#rows'] = $rows;
    $content[$sid]['list']['#header'] = $header;
    $content[$sid]['list']['#theme'] = 'table';
  }
  return $content;
}
