<?php
/**
* @file
* Functions for updating user settings.
*/

function congress_user_settings_tab($user) {

  $content = array();

  $content['form'] = drupal_get_form('congress_user_settings_form', $user);

  return $content;
}
 
/**
*  Form for updating congress details.
*/
function congress_user_settings_form($form, &$form_state, $account) {

  global $user;
  $form['#user'] = $account;
  $form_state['user'] = $account;

  $subs = congress_user_subs($account->uid);
  $form_state['congress_subs'] = $subs;
  $nsubs = count($subs);
  $congress_data = congress_user_get_defaults($account->uid);
  $form_state['congress_data'] = $congress_data;

  $form['subs'] = array(
   '#title' => t('Receive emails for congress'),
   '#type'  => 'fieldset',
   '#weight' => 3,
// '#attributes' => array( 'class' => array('entry-player-fieldset'),),
   '#description' => t('Uncheck to unsubscribe from emails regarding new congresses.'),
  );

  $form['subs']['tabletop'] = array(
     '#markup' => '<table class="congress-table">',
     '#type'  => 'markup',
  );

  $odd = 'even';
  // For each subscription
  foreach($subs as $rid => $sub) {
    if($odd == 'odd') {
      $odd = 'even';
    } else {
      $odd = 'odd';
    }

    $fieldName = 'label' . $rid;
    $orgName = $sub['name'];
    $form['subs'][$fieldName] = array(
       '#markup' => "<tr><th>$orgName</th><td>",
       '#type'  => 'markup',
    );

    $fieldName = 'check' . $rid;

    $form['subs'][$fieldName] = array(
       '#type'  => 'checkbox',
       '#default_value'  => $sub['sub'],
       '#suffix' => '</td></tr>',
    );

    if($user->uid != $account->uid && !user_tools_access($rid,'O','U') ) {
      $form['subs'][$fieldName]['#attributes'] = array('readonly' => 'readonly');
    }

  }

  $form['subs']['tablebottom'] = array(
     '#markup' => '</table>',
     '#type'  => 'markup',
  );

  if($congress_data->uid == 0) {
    $congress_data->first_name = ' ';
    $congress_data->last_name = ' ';
    $congress_data->ecfcode = ' ';
    $congress_data->fidecode = ' ';
    $congress_data->address1 = ' ';
    $congress_data->address2 = ' ';
    $congress_data->city = ' ';
    $congress_data->postcode = ' ';
    $congress_data->telephone = ' ';
    $congress_data->ecfcode = ' ';
  }

  if($account->uid == $user->uid) {
    $form['congress'] = array(
     '#title' => t('Congress Defaults'),
     '#type'  => 'fieldset',
     '#weight' => 3,
     '#attributes' => array( 'class' => array('entry-player-fieldset'),),
     '#description' => t('These details will be used as defaults when you enter a congress.'),
    );

    $form['congress']['first_name'] = array(
     '#type'  => 'textfield',
     '#size'  => 10,
     '#maxlength'  => 10,
     '#title'  => t('First Name'),
     '#required' => true,
     '#default_value'  => $congress_data->first_name,
     '#weight' => 10,
    );

    $form['congress']['last_name'] = array(
     '#type'  => 'textfield',
     '#size'  => 10,
     '#maxlength'  => 30,
     '#title'  => t('Last Name'),
     '#required' => true,
     '#default_value'  => $congress_data->last_name,
     '#weight' => 20,
    );

    $form['congress']['address1'] = array(
     '#type'  => 'textfield',
     '#size'  => 30,
     '#maxlength'  => 30,
     '#title'  => t('Address First line'),
     '#default_value'  => $congress_data->address1,
     '#weight' => 30,
    );

    $form['congress']['address2'] = array(
     '#type'  => 'textfield',
     '#size'  => 30,
     '#maxlength'  => 30,
     '#title'  => t('Address Second line'),
     '#default_value'  => $congress_data->address2,
     '#weight' => 40,
    );

    $form['congress']['city'] = array(
     '#type'  => 'textfield',
     '#size'  => 30,
     '#maxlength'  => 30,
     '#title'  => t('City'),
     '#default_value'  => $congress_data->city,
     '#weight' => 45,
    );

    $form['congress']['postcode'] = array(
     '#type'  => 'textfield',
     '#size'  => 10,
     '#maxlength'  => 10,
     '#title'  => t('Post Code'),
     '#default_value'  => $congress_data->postcode,
     '#weight' => 50,
    );

    $form['congress']['telephone'] = array(
     '#type'  => 'textfield',
     '#size'  => 20,
     '#maxlength'  => 20,
     '#title'  => t('Telephone Number'),
     '#default_value'  => $congress_data->telephone,
     '#weight' => 60,
    );

    $form['congress']['ecfcode'] = array(
     '#type'  => 'textfield',
     '#size'  => 20,
     '#maxlength'  => 7,
     '#title'  => t('ECF Grading Code'),
     '#default_value'  => $congress_data->ecfcode,
     '#weight' => 70,
    );
  }

  $form['submit'] = array(
     '#value' => t('Save'),
     '#type'  => 'submit',
     '#weight' => 1,
  );

  return $form;
}

/**
*  Action after the form is submitted.
*/
function congress_user_settings_form_submit($form, &$form_state) {
  global $user;
  $congress_data = $form_state['congress_data'];
  $subs = $form_state['congress_subs'];
  $account = $form_state['user'];
  // update defaults record for user
  if($user->uid == $account->uid ) {
    $congress_data->first_name = $form_state['values']['first_name'];
    $congress_data->last_name = $form_state['values']['last_name'];
    $congress_data->ecfcode = $form_state['values']['ecfcode'];
    $congress_data->address1 = $form_state['values']['address1'];
    $congress_data->address2 = $form_state['values']['address2'];
    $congress_data->city = $form_state['values']['city'];
    $congress_data->postcode = $form_state['values']['postcode'];
    $congress_data->telephone = $form_state['values']['telephone'];
    if($congress_data->uid == 0) {
      $congress_data->uid = $account->uid;
      congress_user_add_defaults($congress_data);
    } else {
      congress_user_set_defaults($congress_data);
    }
  }

  // update subscriptions for user
  foreach($subs as $rid => $sub) {
    if($user->uid == $account->uid || user_tools_access($rid,'O','U') ) {
      $fieldName = 'check' . $rid;
      $subscription = $form_state['values'][$fieldName];
      $congress_user_email = (object) array(
         'uid' => $account->uid,
         'rid' => $rid,
         'subscription' => $subscription,
      );
      $current_sub = congress_user_subs($account->uid,$rid);
      if(count($current_sub) == 0 ) {
        $status = drupal_write_record('congress_user_email', $congress_user_email);
      } else {
        $status = drupal_write_record('congress_user_email', $congress_user_email, array('uid', 'rid'));
      }
      if(!$status) {
        drupal_set_message('Update Failed', 'error');
      }
    }
  }
  drupal_set_message('Defaults Saved');
}

function congress_unsubscribe($user) {
  $uid = $user->uid;
  $url = "user/$uid/congress";
  drupal_goto($url);
}
