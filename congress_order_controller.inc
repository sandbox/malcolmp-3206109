<?php
/**
 * @file
 * Congress Order controller
 */

class CongressOrderController extends DrupalDefaultEntityController {
  public function create() {
    return (object) array(
      'oid' => '',
      'price' => '0',
      'status' => 'CREATED',
    );
  }

  public function load($ids = array(), $conditions = array()) {
    $objects = parent::load($ids, $conditions);
    return $objects;
  }

  public function save($order) {
    $transaction = db_transaction();

    try {
      $order->is_new = empty($order->oid);
      if($order->is_new) {
        drupal_write_record('congress_order', $order);
        $op = 'insert';
      } else {
        drupal_write_record('congress_order', $order, 'oid');
        $op = 'update';
      }
      // Save fields
      $function = 'field_attach_' . $op;
      $function('congress_order', $order);

      module_invoke_all('entity_' . $op, $order, 'congresss_order');
      unset($order->is_new);

      db_ignore_slave();

      return $order;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('congress', $e, NULL, WATCHDOG_ERROR);
      return FALSE;
    }
  }

  public function delete($oids) {
    if(!empty($oids)) {
      $entries = $this->load($oids, array());
      $transaction = db_transaction();

      try {
        db_delete('congress_order')
          ->condition('oid', $oids, 'IN')
          ->execute();

        foreach($entries as $oid => $order) {
          field_attach_delete('congress_order', $order);
        }

        db_ignore_slave();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('congress', $e, NULL, WATCHDOG_ERROR);
        return FALSE;
      }

      module_invoke_all('entity_delete', $order, 'congress_order');

      cache_clear_all();
      $this->resetCache();
    }
    
    return TRUE;
  }
}

