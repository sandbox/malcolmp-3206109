<?php
/**
* @file
* Functions for updating a congress.
*/

function congress_add() {

  $congress = entity_get_controller('congress')->create();
  return drupal_get_form('congress_update_form', $congress);
}
 
/**
*  Form for updating congress details.
*/
function congress_update_form($form, &$form_state, $congress) {

  $form['#congress'] = $congress;
  $form_state['congress'] = $congress;

  if(empty($congress->cid)) {
    $btitle = 'Add Congress';
  } else {
    $btitle = 'Update Congress';
  }

  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
     '#value' => t($btitle),
     '#type'  => 'submit',
     '#weight' => 5,
  );
  if(!empty($congress->cid)) {
    $form['buttons']['delete'] = array(
       '#value' => t('Delete Congress'),
       '#type'  => 'submit',
       '#submit'  => array('congress_update_delete'),
       '#weight' => 15,
    );
  }
  $form['buttons']['add_section'] = array(
     '#value' => t('Add Section'),
     '#type'  => 'submit',
     '#weight' => 30,
  );
  $form['buttons']['cancel'] = array(
    '#value' => t('Cancel'),
    '#type'  => 'submit',
    '#submit'  => array('congress_update_cancel'),
    '#weight' => 25,
  );
  $form['buttons']['clone'] = array(
    '#value' => t('Clone Congress'),
    '#type'  => 'submit',
    '#submit'  => array('congress_update_clone'),
    '#weight' => 25,
  );

  // Congress options field set
  $form['options'] = array(
   '#title' => t('Congress Options'),
   '#type'  => 'fieldset',
   '#weight' => 12,
   '#attributes' => array(
                     'class' => array('congress-options-fieldset'),
                    )
  );

  // Congress name
  $form['options']['name'] = array(
   '#type'  => 'textfield',
   '#size'  => 25,
   '#title'  => t('Name of Congress'),
   '#default_value'  => $congress->name,
   '#maxlength'  => 30,
   '#weight' => 9,
   '#required' => true,
  );

  // Congress date
  $form['options']['date'] = array(
   '#type'  => 'date_popup',
   '#title'  => t('Date Of Congress'),
   '#date_format'  => 'Y-m-d',
   '#date_label_position'  => 'within',
   '#date_year_range' => '-1:+3',
   '#weight' => 8,
  );
  if(isset($congress->date) ) {
    $form['options']['date']['#default_value'] = date('Y-m-d',$congress->date);
  }

  // Congress organisation
  $roles = congress_list_users_roles();
  $form['options']['role'] = array(
   '#title'  => t('Congress Organisation'),
   '#type'  => 'select',
   '#options' => $roles,
   '#default_value'  => $congress->rid,
   '#weight' => 10,
  );

  // Congress logo
  $logos = congress_list_logos();
  $form['options']['logo'] = array(
   '#title'  => t('Image File'),
   '#type'  => 'select',
   '#options' => $logos,
   '#default_value'  => $congress->logo,
   '#weight' => 11,
  );

  // Congress status
  $stats = array(
    'A' => t('Active'),
    'H' => t('Hidden'),
    'I' => t('Inactive'),
  );

  $form['options']['status'] = array(
   '#title'  => t('Status of Congress'),
   '#type'  => 'select',
   '#options'  => $stats,
   '#default_value'  => $congress->status,
   '#weight' => 25,
  );

  // Congress platform
  $popts = congress_platforms();
  $form['options']['platform'] = array(
   '#type'  => 'select',
   '#options' => $popts,
   '#title'  => t('Platform'),
   '#default_value'  => $congress->platform,
   '#weight' => 22,
  );

  // Entry form style
  $sopts = congress_style_options();
  $form['options']['style'] = array(
   '#type'  => 'select',
   '#options' => $sopts,
   '#title'  => t('Entry Form Style'),
   '#default_value'  => $congress->style,
   '#weight' => 23,
  );

  // Coongress Country
  $countries = congress_get_countries();
  $form['options']['country'] = array(
   '#title'  => t('Country hosting the congress'),
   '#type'  => 'select',
   '#options' => $countries,
   '#default_value'  => $congress->country,
   '#weight' => 24,
  );

  // Email recipients
  $form['options']['email'] = array(
   '#type'  => 'textfield',
   '#size'  => 50,
   '#title'  => t('Email Recipients (comma seperated)'),
   '#default_value'  => $congress->email,
   '#maxlength'  => 200,
   '#weight' => 17,
  );

  // Ask if femail
  $fopts = array(
    'N' => t('No'),
    'Y' => t('Yes'),
  );
  $form['options']['ask_female'] = array(
   '#type'  => 'select',
   '#options' => $fopts,
   '#title'  => t('Ask if female'),
   '#default_value'  => $congress->ask_female,
   '#weight' => 22,
  );

  $form['options']['max_entrants'] = array(
   '#type'  => 'textfield',
   '#size'  => 10,
   '#title'  => t('Maximum number of entries'),
   '#description'  => t('Zero for unlimited'),
   '#default_value'  => $congress->max_entrants,
   '#weight' => 27,
   '#required' => true,
  );

  $form['options']['multiple_sections'] = array(
   '#type'  => 'checkbox',
   '#title'  => t('Allow entry of multiple sections'),
   '#default_value'  => $congress->multiple_sections,
   '#weight' => 34,
  );

  // Payment options field set
  $form['pay'] = array(
   '#title' => t('Payment Options'),
   '#type'  => 'fieldset',
   '#weight' => 12,
   '#attributes' => array(
                     'class' => array('congress-pay-fieldset'),
                    )
  );

  // Paypal
  $payments = array(0 => '(NONE)');
  $payments += congress_list_payment_methods();
  $form['pay']['payment'] = array(
   '#title'  => t('Paypal Payment Method'),
   '#type'  => 'select',
   '#options' => $payments,
   '#default_value'  => $congress->pmid,
   '#weight' => 10,
  );

  // Bank transfer
  $banks = array('N' => 'No', 'Y' => 'Yes');
  $form['pay']['bank_option'] = array(
   '#type'  => 'select',
   '#options' => $banks,
   '#title'  => t('Allow payment by bank transfer'),
   '#default_value'  => $congress->bank_option,
   '#weight' => 20,
  );

  $form['pay']['bank_text'] = array(
   '#type'  => 'textarea',
   '#rows'  => 4,
   '#cols'  => 40,
   '#title'  => t('Bank payment instructions'),
   '#description'  => t('Include sort code, account number, any anything else such as reference instructions. Up to 128 characters'),
   '#default_value'  => $congress->bank_text,
   '#maxlength'  => 128,
   '#weight' => 20,
  );

  $form['late'] = array(
   '#title' => t('Late Entries'),
   '#type'  => 'fieldset',
   '#weight' => 13,
   '#attributes' => array(
                     'class' => array('congress-late-fieldset'),
                    )
  );

  $form['late']['late_date'] = array(
   '#type'  => 'date_popup',
   '#title'  => t('Late Entry Date'),
   '#description'  => t('The last day you can enter before the late entry fee becomes payable.'),
   '#date_format'  => 'Y-m-d',
   '#date_label_position'  => 'within',
   '#date_year_range' => '-1:+3',
   '#weight' => 5,
  );
  if(isset($congress->late_date) ) {
    $form['late']['late_date']['#default_value'] = date('Y-m-d',$congress->late_date);
  }

  $form['late']['late_fee'] = array(
   '#type'  => 'textfield',
   '#size'  => 10,
   '#title'  => t('Late Entry Fee'),
   '#default_value'  => $congress->late_fee,
   '#weight' => 10,
   '#maxlength'  => 8,
   '#required' => true,
  );

  $form['late']['late_date2'] = array(
   '#type'  => 'date_popup',
   '#title'  => t('Late Entry Date2'),
   '#date_format'  => 'Y-m-d H:i:s',
   '#date_label_position'  => 'within',
   '#date_type' => DATE_UNIX,
   '#date_year_range' => '-1:+3',
   '#weight' => 15,
  );
  if(isset($congress->late_date2) ) {
    $form['late']['late_date2']['#default_value'] = date('Y-m-d H:i:s',$congress->late_date2);
  }

  $form['late']['late_fee2'] = array(
   '#type'  => 'textfield',
   '#size'  => 10,
   '#title'  => t('Late Entry Fee2'),
   '#default_value'  => $congress->late_fee2,
   '#maxlength'  => 8,
   '#weight' => 20,
   '#required' => true,
  );

  $form['late']['entry_closed_days'] = array(
   '#type'  => 'textfield',
   '#size'  => 11,
   '#title'  => t('Online entry closing day'),
   '#description'  => t('Number of days before congress when online entry closes. 0 days means that no online entry accepted on the day of the congress, but are accepted the day before. If you want to allow entry on the day of the congress put -1'),
   '#default_value'  => $congress->entry_closed_days,
   '#weight' => 25,
   '#required' => true,
  );

  // Date of birth fieldset
  $form['dob'] = array(
   '#title' => t('Date Of Birth'),
   '#type'  => 'fieldset',
   '#weight' => 13,
   '#attributes' => array(
                     'class' => array('congress-dob-fieldset'),
                    )
  );


  $dobs = array(
    'N' => t('No'),
    'O' => t('Optional'),
    'M' => t('Mandatory'),
    'Y' => t('Year Of Birth'),
  );

  $form['dob']['ask_dob'] = array(
   '#type'  => 'radios',
   '#options'  => $dobs,
   '#title'  => t('Request Date Of Birth'),
   '#default_value'  => $congress->ask_dob,
   '#weight' => 10,
  );

  $form['dob']['dob_prompt'] = array(
   '#type'  => 'textfield',
   '#size'  => 40,
   '#title'  => t('DOB explanatory text'),
   '#default_value'  => $congress->dob_prompt,
   '#maxlength'  => 40,
   '#weight' => 20,
  );

  $form['dob']['age_date'] = array(
   '#type'  => 'date_popup',
   '#title'  => t('Date for determining age'),
   '#date_format'  => 'Y-m-d',
   '#date_label_position'  => 'within',
   '#date_year_range' => '-1:+3',
   '#weight' => 30,
  );
  if(isset($congress->age_date) ) {
    $form['dob']['age_date']['#default_value'] = date('Y-m-d',$congress->age_date);
  }

  $form['dob']['junior_discount'] = array(
   '#type'  => 'textfield',
   '#size'  => 10,
   '#title'  => t('Junior Discount'),
   '#default_value'  => $congress->junior_discount,
   '#maxlength'  => 8,
   '#weight' => 40,
   '#required' => true,
  );

  $form['dob']['vetran_discount'] = array(
   '#type'  => 'textfield',
   '#size'  => 10,
   '#title'  => t('Veteran Discount'),
   '#default_value'  => $congress->vetran_discount,
   '#maxlength'  => 8,
   '#weight' => 50,
   '#required' => true,
  );

  // Discounts field set
  $form['discounts'] = array(
   '#title' => t('Discounts'),
   '#type'  => 'fieldset',
   '#weight' => 12,
   '#attributes' => array(
                     'class' => array('congress-discount-fieldset'),
                    )
  );

  $form['discounts']['multiple_discount'] = array(
   '#type'  => 'textfield',
   '#size'  => 10,
   '#title'  => t('Multiple Entry Discount'),
   '#default_value'  => $congress->multiple_discount,
   '#required' =>TRUE,
   '#maxlength'  => 8,
   '#weight' => 35,
  );

  $mds = array(
    'Y' => t('Standard'),
    'N' => t('None'),
    'F' => t('Members Only'),
    '1' => t('£ 1'),
    '2' => t('£ 2'),
    '3' => t('£ 3'),
    '4' => t('£ 4'),
  );

  $form['discounts']['member_discount'] = array(
   '#title'  => t('Give members discounts'),
   '#type'  => 'select',
   '#options'  => $mds,
   '#default_value'  => $congress->member_discount,
   '#weight' => 26,
  );


  $form['discounts']['extra_discount'] = array(
   '#type'  => 'textfield',
   '#size'  => 10,
   '#title'  => t('Extra Discount'),
   '#default_value'  => $congress->extra_discount,
   '#maxlength'  => 8,
   '#weight' => 28,
   '#required' => true,
  );

  $form['discounts']['extra_discount_text'] = array(
   '#type'  => 'textfield',
   '#size'  => 30,
   '#title'  => t('Extra Discount Description'),
   '#default_value'  => $congress->extra_discount_text,
   '#maxlength'  => 30,
   '#weight' => 29,
  );

  $form['extra_question'] = array(
   '#type'  => 'textfield',
   '#size'  => 30,
   '#title'  => t('Extra Question'),
   '#description'  => t('The answer can only be 16 characters'),
   '#default_value'  => $congress->extra_question,
   '#maxlength'  => 30,
   '#weight' => 31,
  );

  $form['entrants_text'] = array(
   '#type'  => 'textarea',
   '#rows'  => 2,
   '#cols'  => 50,
   '#title'  => t('Text heading on the entrants list'),
   '#default_value'  => $congress->entrants_text,
   '#maxlength'  => 100,
   '#weight' => 32,
  );

  $form['entry_text'] = array(
   '#type'  => 'textarea',
   '#rows'  => 10,
   '#cols'  => 50,
   '#title'  => t('Explanatory text seen by user on the home tab'),
   '#default_value'  => $congress->entry_text,
   '#weight' => 33,
  );

  // Sections

  $form['sections'] = array(
     '#type'  => 'markup',
     '#prefix' => '<table border><tr><th>Section</th><th>Entry Fee</th><th>Grade Limit</th><th>Rating</th><th>Rate</th><th>Bye Rounds</th><th>Byes</th><th>Age</th><th>Grp</th><th>Delete</th></tr>',
     '#weight' => 40,
     '#suffix' => '</table><p>To remove a section, check delete column and press update congress button<p>',
  );

  $options = congress_rating_type_options($congress->country);
  $speeds = congress_speed_options();
  $weight = 15;
  foreach($congress->sections as $section) {
    $sid = $section->sid;

    $weight++;
    $form['sections']['section'.$sid] = array(
     '#type'  => 'textfield',
     '#size'  => 10,
     '#default_value'  => $section->name,
     '#maxlength'  => 30,
     '#weight' => $weight,
     '#prefix' => '<tr><td>',
     '#suffix' => '</td>',
    );

    $weight++;
    $form['sections']['entry_fee'.$sid] = array(
     '#type'  => 'textfield',
     '#size'  => 5,
     '#default_value'  => $section->entry_fee,
     '#maxlength'  => 8,
     '#weight' => $weight,
     '#prefix' => '<td>',
     '#suffix' => '</td>',
    );

    $weight++;
    $form['sections']['grade_limit'.$sid] = array(
     '#type'  => 'textfield',
     '#size'  => 5,
     '#default_value'  => $section->grade_limit,
     '#weight' => $weight,
     '#prefix' => '<td>',
     '#suffix' => '</td>',
    );

    $weight++;
    $form['sections']['rating_type'.$sid] = array(
     '#type'  => 'select',
     '#options' => $options,
     '#default_value'  => $section->rating_type,
     '#weight' => $weight,
     '#prefix' => '<td>',
     '#suffix' => '</td>',
    );

    $weight++;
    $form['sections']['speed'.$sid] = array(
     '#type'  => 'select',
     '#options' => $speeds,
     '#default_value'  => $section->speed,
     '#weight' => $weight,
     '#prefix' => '<td>',
     '#suffix' => '</td>',
    );

    $weight++;
    $form['sections']['rounds'.$sid] = array(
     '#type'  => 'textfield',
     '#size'  => 3,
     '#default_value'  => $section->rounds,
     '#weight' => $weight,
     '#prefix' => '<td>',
     '#suffix' => '</td>',
    );

    $weight++;
    $form['sections']['byes'.$sid] = array(
     '#type'  => 'textfield',
     '#size'  => 3,
     '#default_value'  => $section->byes,
     '#weight' => $weight,
     '#prefix' => '<td>',
     '#suffix' => '</td>',
    );

    $weight++;
    $form['sections']['age'.$sid] = array(
     '#type'  => 'textfield',
     '#size'  => 3,
     '#default_value'  => $section->age,
     '#weight' => $weight,
     '#prefix' => '<td>',
     '#suffix' => '</td>',
    );

    $weight++;
    $form['sections']['grp'.$sid] = array(
     '#type'  => 'textfield',
     '#size'  => 3,
     '#default_value'  => $section->grp,
     '#weight' => $weight,
     '#prefix' => '<td>',
     '#suffix' => '</td>',
    );

    $weight++;
    $form['sections']['del'.$sid] = array(
     '#type'  => 'checkbox',
     '#weight' => $weight,
     '#prefix' => '<td>',
     '#suffix' => '</td></tr>',
    );
  }

  field_attach_form('congress', $congress, $form, $form_state);

  return $form;
}


/**
*  Action after the cancel button is pressed.
*/
function congress_update_cancel($form, &$form_state) {
  drupal_goto('admin/content/congress');
}

/**
*  Action after the clone button is pressed.
*/
function congress_update_clone($form, &$form_state) {
  $congress = $form_state['congress'];
  drupal_set_message("Cloning congress $congress->cid");
  // Blank out id so new one created
  $congress->cid = '';
  // Temporarily store sections
  $sections = $congress->sections;
  $congress->sections = array();
  // Rename
  $new_name = substr('NEW ' . $congress->name, 0, 30);
  $congress->name = $new_name;
  // Save congress
  congress_save($congress);
  // Add sections back in under new id
  foreach($sections as $key => $section) {
    $section->sid = '';
    $section->cid = $congress->cid;
  }
  $congress->sections = $sections;
  congress_save($congress);

  drupal_set_message("Created new congress $congress->name");
  drupal_goto('congress/' . $congress->cid . '/edit');
}

/**
*  Action after the delete button is pressed.
*/
function congress_update_delete($form, &$form_state) {
  $congress = $form_state['congress'];
  $num_entries = congress_total_entry_count($congress->cid);
  if($num_entries == 0 ) {
    drupal_set_message("Deleting congress $congress->cid ...");
    if(congress_delete($congress->cid) ) {
      drupal_set_message('Congress deleted');
    } else {
      drupal_set_message('Congress delete failed', 'error');
    }
    drupal_goto('congress/mine');
  } else {
    drupal_set_message("Cannot delete congress because it has $num_entries entries: ", 'error' );
    drupal_goto('congress/' . $congress->cid . '/edit');
  }
}
/**
*  Validate congress update.
*/
function congress_update_form_validate($form, &$form_state) {
  // Payment options
  $bank_option = $form_state['values']['bank_option'];
  $bank_text = $form_state['values']['bank_text'];
  $paypal = $form_state['values']['payment'];
  if( $paypal == 0 && $bank_option == 'N' ) {
    form_set_error('payment', "You must select either a paypal payment method or bank transfer");
  }
  if( $bank_option == 'Y' && strlen($bank_text) < 3 ) {
    form_set_error('bank_text', "You must enter account number and sort code instructions");
  }

  // Junior discount
  $junior_discount = $form_state['values']['junior_discount'];
  if(isset($junior_discount) && strlen($junior_discount) > 0 ) {
    if(!is_numeric($junior_discount) ) {
      form_set_error('junior_discount', "Error: junior discount must be numeric");
    } else {
      $age_date = $form_state['values']['age_date'];
      if(!isset($age_date) && $junior_discount>0) {
        form_set_error('age_date', "Error: If having junior discount must specify age date");
      }
    }
  }

  // Multiple discount
  $multiple_discount = $form_state['values']['multiple_discount'];
  if(isset($multiple_discount) && strlen($multiple_discount) > 0 ) {
    if(!is_numeric($multiple_discount) ) {
      form_set_error('multiple_discount', "Error: multiple discount must be numeric");
    }
  }
  $email = $form_state['values']['email'];
  $adminMails = explode( ',' , $email);
  foreach($adminMails as $mail) {
    if( ! valid_email_address($mail) ) {
      form_set_error('email', "One of the email addresses is invalid. Do not end the list with a commma, ");
    }
  }
  $time = time();
  // congress date
  $date = $form_state['values']['date'];
  if(isset($date)) {
    $dtime = strtotime($date . ' UTC');
    if( $date < time() ) {
      drupal_set_message("Congress date $date is in the past", 'warning');
    }
    // late entry date
    $late_date = $form_state['values']['late_date'];
    if(isset($late_date)) {
      $ltime = strtotime($late_date . ' UTC');
      if( $ltime > $dtime ) {
        form_set_error('late_date', "Late entry date is after congress date.");
      }
      if( $ltime < time() ) {
        drupal_set_message("Late entry date $late_date is in the past", 'warning');
      }
    }
    $late_date2 = $form_state['values']['late_date2'];
    if(isset($late_date2)) {
      $ltime2 = strtotime($late_date2 . ' UTC');
      if( $ltime2 > $dtime ) {
        form_set_error('late_date2', "Late entry date 2 is after congress date.");
      }
      if( $ltime2 < time() ) {
        drupal_set_message("Late entry date 2 $late_date2 is in the past", 'warning');
      }
    }
  }
}

/**
*  Action after the form is submitted.
*  Update congress record
*/
function congress_update_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $congress = $form_state['congress'];
  $congress->name = $form_state['values']['name'];
  $congress->pmid = $form_state['values']['payment'];
  $congress->rid = $form_state['values']['role'];
  $congress->logo = $form_state['values']['logo'];
  $congress->email = $form_state['values']['email'];
  $date = $form_state['values']['date'];
  if(isset($date)) {
    $congress->date = strtotime($date . ' UTC');
  } else {
    $congress->date = null;
  }
  $congress->junior_discount = $form_state['values']['junior_discount'];
  $congress->vetran_discount = $form_state['values']['vetran_discount'];
  $congress->ask_dob = $form_state['values']['ask_dob'];
  $congress->dob_prompt = $form_state['values']['dob_prompt'];
  $congress->ask_female = $form_state['values']['ask_female'];
  $congress->platform = $form_state['values']['platform'];
  $congress->style = $form_state['values']['style'];
  $congress->member_discount = $form_state['values']['member_discount'];
  $congress->late_fee = $form_state['values']['late_fee'];
  $congress->late_fee2 = $form_state['values']['late_fee2'];
  $congress->entry_text = $form_state['values']['entry_text'];
  $congress->entrants_text = $form_state['values']['entrants_text'];
  $congress->country = $form_state['values']['country'];
  $congress->status = $form_state['values']['status'];
  $congress->max_entrants = $form_state['values']['max_entrants'];
  $congress->entry_closed_days = $form_state['values']['entry_closed_days'];
  $congress->multiple_discount = $form_state['values']['multiple_discount'];
  $congress->extra_discount = $form_state['values']['extra_discount'];
  $congress->extra_discount_text = $form_state['values']['extra_discount_text'];
  $congress->extra_question = $form_state['values']['extra_question'];
  $congress->multiple_sections = $form_state['values']['multiple_sections'];
  $congress->bank_option = $form_state['values']['bank_option'];
  $congress->bank_text = $form_state['values']['bank_text'];

  $late_date = $form_state['values']['late_date'];
  if(isset($late_date)) {
    $congress->late_date = strtotime($late_date . ' UTC');
  } else {
    $congress->late_date = null;
  }
  $late_date2 = $form_state['values']['late_date2'];
  if(isset($late_date2)) {
    $congress->late_date2 = strtotime($late_date2 . ' UTC');
  } else {
    $congress->late_date2 = null;
  }

  $age_date = $form_state['values']['age_date'];
  if(isset($age_date)) {
    $congress->age_date = strtotime($age_date . ' UTC');
  } else {
    $congress->age_date = null;
  }

  // Notify field widgets
  field_attach_submit('congress', $congress, $form, $form_state);

  // Clear caches for all sections incase the gradings have changed.
  foreach($congress->sections as $section) {
    $sid = $section->sid;
    congress_entrants_cache_clear($congress->cid, $sid);
  }

  // Store sections
  foreach($congress->sections as $key => $section) {
    $sid = $section->sid;
    $name = $form_state['values']['section'.$sid];
    $rating_type = $form_state['values']['rating_type'.$sid];
    if( isset($form_state['values']['entry_fee'.$sid]) ) {
      $entry_fee = $form_state['values']['entry_fee'.$sid];
    } else {
      $entry_fee = 0;
    }
    $grade_limit = $form_state['values']['grade_limit'.$sid];
    $speed = $form_state['values']['speed'.$sid];
    $rounds = $form_state['values']['rounds'.$sid];
    $byes = $form_state['values']['byes'.$sid];
    $grp = $form_state['values']['grp'.$sid];
    $age = $form_state['values']['age'.$sid];
    $del = $form_state['values']['del'.$sid];
    if($del == 1) {
      unset($congress->sections[$key]);
      db_delete('congress_section')->condition('sid', $sid)->execute();
    } else {
      $section->name = $name;
      $section->entry_fee = $entry_fee;
      $section->grade_limit = $grade_limit;
      $section->rating_type = $rating_type;
      $section->speed = $speed;
      $section->rounds = $rounds;
      $section->byes = $byes;
      $section->grp = $grp;
      $section->age = $age;
    }
  }


  // Add section if that button was pressed
  if( $op == 'Add Section' ) {
    drupal_set_message("Adding section");
    $congress->sections[] = entity_get_controller('congress')->section($congress->cid);
  }

  // Save congress
  if( congress_save($congress) ) {
    drupal_set_message(t('Congress Saved'));
  } else {
    drupal_set_message(t('Error saving Congress'),'error');
  }
  if( $op == 'Add Section' ) {
    drupal_goto('congress/' . $congress->cid . '/edit');
  } else {
    drupal_goto('congress/' . $congress->cid . '/admin' );
  }
}

function congress_edit_page($congress) {
  // Show form
  return drupal_get_form('congress_update_form', $congress);
}

function congress_total_entry_count($cid) {
  $query = db_select('congress_entry', 'e');
  $query->condition('e.cid', $cid);
  $num_rows = $query->countQuery()->execute()->fetchField();
  return $num_rows;
}
