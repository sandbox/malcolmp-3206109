<?php
// $Id$
/**
 * @file
 * Functions for my congresses page
 */

/**
 * display content of my congresses page
 */
function congress_my_congresses_page() {
  $content = array();
  $content['note'] = array(
   '#type' => 'markup',
   '#markup' => "<div>Pick on name to get a list of congresses</div>",
  );
  $table = congress_my_list();
  $rows = $table['data'];     // Actual table data
  $header = $table['header']; // Headers
  $content['list']['#rows'] = $rows;
  $content['list']['#header'] = $header;
  $content['list']['#theme'] = 'table';
  return $content;
}

function congress_my_list() {
  
  $header = array();
  $header[] = array('data' => 'Name', 'field' => 'name', 'sort'=>'asc' );
  $header[] = array('data' => 'Users ',   'field' => 'dob' );

  global $user;
  $rows = array();
  $query = db_select('congress_organisation', 'c');
  $query->fields('c', array('rid'));
  $query->innerJoin('user_tools_owner', 'r', 'r.id = c.rid');
  $query->addField('c', 'name', 'name');
  $query->condition('r.uid', $user->uid);
  $query->innerJoin('user_tools_owner', 't2', 't2.id = c.rid');
  $query->innerJoin('users', 'u2', 'u2.uid = t2.uid');
  $query->addField('u2', 'name', 'username');
  $query->addField('u2', 'uid', 'uid');
  $query->distinct();

//print out query
//dsm((string)$query);
  $result = $query->execute();
  $roles = array();
  foreach ( $result as $row) {
    $rid = $row->rid;
    $name = $row->name;
    $roles[$rid]['name'] = $name;
    $link = l($row->username, "user/$row->uid");
    if(isset($roles[$rid]['users']) ) {
      $roles[$rid]['users'] .= " $link";
    } else {
      $roles[$rid]['users'] = $link;
    }
  }
  foreach( $roles as $rid => $role ) {
    $link = l($roles[$rid]['name'], "congress/admin/$rid");
    $newrow = array( $link,$roles[$rid]['users']);
    $rows[] = $newrow;
  }
  $table = array(
    'header' => $header,
    'data' => $rows,
  );

  return $table;
}
