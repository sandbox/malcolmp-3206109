<?php
/**
* @file
* Functions for deleting a congress entry.
*/

function congress_entry_delete_page($entry) {
  $cid = $entry->cid;
  drupal_set_message("Deleting congress entry ". $entry->eid);
  if(congress_entry_delete($entry->eid)) {
    drupal_set_message("Congress entry deleted");
  } else {
    drupal_set_message("Congress entry delete failed");
  }
  drupal_goto('congress/' . $cid . '/enter');
}
