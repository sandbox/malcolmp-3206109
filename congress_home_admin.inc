<?php
// $Id$
/**
 * @file
 * Functions for congress home tabs
 */

/**
 * display content of congress home tab
 */
function congress_admin_page($congress) {
  drupal_set_title(' ');
  $content = array();

  $status = arg(3);
  if(!isset($status) ) {
    $status = 'ALL_ENTERED';
  }
  $table = congress_entries_table($congress,$status);
  $rows = $table['data'];     // Actual table data
  $header = $table['header']; // Headers

  $content['form'] = drupal_get_form('congress_home_admin_form', $congress, count($rows) );
  $content['list']['#rows'] = $rows;
  $content['list']['#header'] = $header;
  $content['list']['#theme'] = 'table';
  return $content;
}

function congress_home_admin_form($form, &$form_state, $congress, $total) {
  $form['#congress'] = $congress;
  $form_state['congress'] = $congress;
  $all = array( 'ALL' => 'ALL', 'ALL_ENTERED' => 'ALL_ENTERED' );
  $states = array_merge($all, congress_entry_states() );

  $status = arg(3);
  if(!isset($status) ) {
    $status = 'ALL_ENTERED';
  }

  $form['status'] = array(
   '#title'  => t('Payment Status Filter'),
   '#type'  => 'select',
   '#options' => $states,
   '#default_value'  => $status,
   '#weight' => 10,
   '#attributes' => array('onchange' => 'this.form.submit();'  ),
  );

  $form['buttons'] = array();
  $form['buttons']['#weight'] = 20;
  $form['buttons']['#prefix'] = '<div id=congress-admin-buttons>';
  $form['buttons']['#suffix'] = '</div>';

  //Entries comment
  $form['buttons']['comment'] = array(
     '#type'  => 'markup',
     '#markup' => "Total : $total ",
     '#weight' => 5,
  );

  //Submit button. (hidden, but needed for form to work)
  $form['buttons']['submit'] = array(
     '#value' => t('Pay'),
     '#type'  => 'submit',
     '#attributes' => array('style' => 'display: none;'),
     '#weight' => 10,
  );

  //Add Entry button
  $form['buttons']['add'] = array(
     '#value' => t('Add Entry'),
     '#submit'  => array('congress_home_admin_add'),
     '#type'  => 'submit',
     '#weight' => 20,
  );

  //XL button
  $form['buttons']['xl'] = array(
     '#value' => t('Entrants List'),
     '#submit'  => array('congress_home_admin_xl'),
     '#type'  => 'submit',
     '#weight' => 30,
  );

  $xls = array(
    'M' => t('Swiss Manager Rating'),
    'S' => t('Standard'),
    'E' => t('Entries'),
  );
  foreach($congress->sections as $section) {
    $xls[$section->sid] = $section->name . ' (Swiss Manager)';
  }
  foreach($congress->sections as $section) {
    $xls['V' . $section->sid] = $section->name . ' (Vega)';
  }

  $form['buttons']['xltype'] = array(
// '#title'  => t('Type'),
   '#type'  => 'select',
   '#options' => $xls,
   '#default_value'  => 'M',
   '#weight' => 35,
  );

  return $form;
}

function congress_home_admin_form_submit($form, &$form_state) {
  $status = $form_state['values']['status'];
  $congress = $form_state['congress'];
  $cid = $congress->cid;
  drupal_goto("congress/$cid/admin/$status");
}

function congress_home_admin_xl($form, &$form_state) {
  $congress = $form_state['congress'];
  $cid = $congress->cid;
  $type = $form_state['values']['xltype'];
  $filter = $form_state['values']['status'];
  drupal_goto("congress/$cid/xl/$type/$filter");
}

function congress_home_admin_add($form, &$form_state) {
  $status = $form_state['values']['status'];
  $congress = $form_state['congress'];
  $cid = $congress->cid;
  drupal_goto("congress_entry/adminadd/$cid");
}

// Table of congress entrants
//  $congress 
//  $status    - ALL / PAID / SUBMITTED / etc

function congress_entries_table($congress, $status) {
  $all_players = [];
  $sections = [];
  foreach($congress->sections as $section) {
    $players = congress_entrants_query($congress, $section, $status);
    foreach($players as $eid => $player) {
      $sections[$eid] = $section->name;
    }
    $all_players = $all_players + $players;
  }

  $rows=[];
  // Get the rows from the query
  foreach ( $all_players as $eid => $player) {
    // Only include those with requested status
    $pstatus = $player['status'];
    $first_name = $player['first_name'];
    $last_name = $player['last_name'];
    $club = $player['club'];
    $section = $sections[$eid];
    $trating = $player['trating'];
    $source = $player['source'];
    $updated = date('Y-m-d', $player['updated']);

    $row = [$first_name, $last_name, $club, $section, $pstatus, $trating, $source, $updated, $eid];
    $rows[] = $row;
  }

  $fnnum = 0;
  $header = [
    ['data' => 'First Name', 'field' => $fnnum++, 'char' => TRUE],
    ['data' => 'Last Name', 'field' => $fnnum++, 'char' => TRUE],
    ['data' => 'Club', 'field' => $fnnum++, 'char' => TRUE],
    ['data' => 'Section', 'field' => $fnnum++, 'char' => TRUE],
    ['data' => 'Status', 'field' => $fnnum++, 'char' => TRUE],
    ['data' => 'Tourney Rating', 'field' => $fnnum++, 'sort' => 'asc', 'char' => FALSE],
    ['data' => 'Rating Source', 'field' => $fnnum++, 'char' => TRUE],
    ['data' => 'Last Updated', 'field' => $fnnum++, 'char' => TRUE],
  ];

  congress_table_sort($rows, $header, 'First Name');

  $final_rows=[];
  foreach($rows as $row) {
    $eid = $row[8];
    $first_name = l($row[0], "congress_entry/$eid/admin");
    $final_rows[] = [$first_name, $row[1], $row[2], $row[3], $row[4], $row[5], $row[6], $row[7]];
  }

  $table = array(
    'header' => $header,
    'data' => $final_rows,
  );

  return $table;
}
