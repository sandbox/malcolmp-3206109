// Javascript for the congress entry page
//

// when the document is loaded ..
(function($){
$(document).ready(function() {
function changeByePoints(e) {
  val = this.value;
  len = this.length;
  if(val == len -1) {
    this.labels[0].innerText = this.labels[0].innerText.replace("Half", "Zero");
  } else {
    this.labels[0].innerText = this.labels[0].innerText.replace("Zero", "Half");
  }
}
// Function to set up form depending on section chosen.
function setSection(e) {
  fidesec=0;
  ecfsec=0;
  if(multiple_sections=='Y') {
    for (cs in congress_sections) {
      // Avoid the dummy zero section 
      if( cs != 0 ) {
        section = congress_sections[cs];
        // Get section setting
        sectid="#edit-sid"+cs;
        secval=$(sectid).get(0);
        rowid="#bye-row-"+cs;
        // If section selected
        if(secval.checked) {
          // Set byes row visible
          if(section.byes>0) {
            $(rowid).get(0).style.display = "";
          } else {
            $(rowid).get(0).style.display = "none";
          }
          // Count FIDE rated sections
          if(section.type == "F") {
            fidesec++;
          }
          // Count sections needing ECF silver discount
          if(section.membership == "Silver" || section.membership == "Gold" || section.membership == "SCO" ) {
            ecfsec++;
          }
        } else {
          // Set byes row not visible
          $(rowid).get(0).style.display = "none";
        }
      }
    }
  } else {
    // Get section selected
    sid=$("#edit-sid").get(0).value;
    section = congress_sections[sid];
    // Set ECF membership appropriately 
    if(section.membership == "Silver" || section.membership == "Gold" || section.membership == "SCO" ) {
      ecfsec=1;
    }
    if(section.type == "F") {
      fidesec=1;
    }

    // Set byes
    for (cs in congress_sections) {
      rowid="#bye-row-"+cs;
      if(cs!=0) {
        if(cs==sid && section.byes>0) {
          $(rowid).get(0).style.display = "";
        } else {
          $(rowid).get(0).style.display = "none";
        }
      }
    }
  }
  // Set visibility of FIDE code and federation
  description = $(".form-item-fidecode .description").get(0);
  row = $("#federation-row").get(0);
  if(fidesec>0) {
    description.textContent = "Required";
    row.style.display = "";
  } else {
    description.textContent = "If FIDE rated but not in grading list";
    row.style.display = "none";
  }
  // Set visibility of ECF Membership
  row = $("#member-row").get(0);
  if(ecfsec>0) {
    row.style.display = "";
  } else {
    row.style.display = "none";
  }
  // Set visibility of Late entry fee
  row = $("#late-row").get(0);
  if(late_entry_fee>0) {
    row.style.display = "";
  } else {
    row.style.display = "none";
  }
  // Set visibility of FIDE text.
  if(fidesec>0) {
    $("#grade-note-text").get(0).style.display = "";
  } else {
    $("#grade-note-text").get(0).style.display = "none";
  }
  // Set price
  addPrice();
}
// Function to clear grade details.
function clearGrade() {
  $("#edit-ecfcode").val("");
  $("#edit-fidecode").val("");
  $("#edit-grade").val("");
  $("#edit-rgrade").val("");
  $("#edit-ecfmemb").val("");
}
// Function to add donation
function addDonation() {
  donationText=$("#edit-donation-fee").get(0).value;
  donation=parseFloat(donationText);
  donation+=0.5;
  $("#edit-donation-fee").get(0).value = donation;
  addPrice();
}
// Function to subtract donation
function subDonation() {
  donationText=$("#edit-donation-fee").get(0).value;
  donation=parseFloat(donationText);
  if(donation>0){
    donation-=0.5;
  }
  $("#edit-donation-fee").get(0).value = donation;
  addPrice();
}
// Function to add up all the selector prices and set the total
function addPrice(e) {
  var totalPrice=0;
  pay_fee=0;
  nsects=0;
  // Donation
  donation=$("#edit-donation-fee").get(0).value;
  totalPrice+=parseFloat(donation);
  // Get silver setting
  silver=$("#edit-silver").get(0).value;
  fide_fee=0;
  // If multiple sections allowed ...
  if(multiple_sections=='Y') {
    // Check each section
    for (cs in congress_sections) {
      // Avoid the dummy zero section 
      if( cs != 0 ) {
        section = congress_sections[cs];
        // Get section setting
        sectid="#edit-sid"+cs;
        feeid="#edit-entry-fee"+cs;
        secval=$(sectid).get(0);
        var entry_fee=0;
        // If section selected
        if(secval.checked) {
          nsects++;
          // section.membership is the level of membership needed to enter
          // the section
  
          // Apply a silver member charge to section
          if(section.membership == "Silver") {
            if(silver=='B' || silver=='N' || silver=='M' || silver=='E' ) {
              pay_fee+=section.silver_charge;
              totalPrice+=parseFloat(section.silver_charge);
            }
            if(silver=='J') {
              pay_fee+=section.junior_charge;
              totalPrice+=parseFloat(section.junior_charge);
            }
          }
          // Apply a gold member charge to section
          //  gold_charge is the ECF Gold member discount for FIDE rated section
          if(section.membership == "Gold") {
            if(silver=='B' || silver=='S' || silver=='N' || silver=='M' || silver=='E' || silver=='J' ) {
              pay_fee+=gold_charge;
              totalPrice+=parseFloat(gold_charge);
            }
          }
          // Chess Scotland Membership
          if(section.membership == "SCO") {
            if(silver=='Y' ) {
              pay_fee+=cs_charge;
              totalPrice+=parseFloat(cs_charge);
            }
          }
          // Add up entry fees for each section selected
          entry_fee=section.fee;
          totalPrice+=parseFloat(entry_fee);
        }
        // Set entry fee in form
        $(feeid).get(0).value = entry_fee;
      }
    }
    // Set pay to play discount
    $("#edit-play-fee").get(0).value = pay_fee;
    // Apply multiple entry discount if selected
    multiple_fee=0;
    if(nsects>1){
      multiple_fee=multiple_discount;
    }
    $("#edit-multiple-fee").get(0).value = multiple_fee;
    totalPrice+=parseFloat(multiple_fee);
  } else {
    // Get section selected
    sid=$("#edit-sid").get(0).value;
    section = congress_sections[sid];
    federation=$("#edit-federation").get(0).value;
    if( federation.length == 0 ) {
      federation="XXX";
    }
    // ECF membership
    if(section.membership == "None") {
      $("#edit-play-fee").get(0).value = 0;
    } else {
      // Chess Scotland Membership
      if(section.membership == "SCO") {
        $("#edit-play-fee").get(0).value = 0;
        if(silver=='Y' ) {
          $("#edit-play-fee").get(0).value = cs_charge;
          totalPrice+=parseFloat(cs_charge);
        }
      // Ordinary ECF rated section - silver
      // or FIDE rated rapidplay
      } else {
        if(section.membership == "Silver") {
          // Non ENG players in FIDE rapid are charged the same as ENG
          if(federation=='ENG' || section.speed=='R') {
            if(silver=='S' || silver=='G' || silver=='P' || silver =='K' || silver=='L') {
              $("#edit-play-fee").get(0).value = 0;
            } else {
              if(silver=='J') {
                $("#edit-play-fee").get(0).value = section.junior_charge;
                totalPrice+=parseFloat(section.junior_charge);
              } else {
                $("#edit-play-fee").get(0).value = section.silver_charge;
                totalPrice+=parseFloat(section.silver_charge);
              }
            }
          } else {
            $("#edit-play-fee").get(0).value = 0;
            if(silver!='G' && silver!='P' && silver!='S' && silver!='K' && silver!='L') {
              fide_fee=fide_charge;
            }
          }
        // FIDE rated section - gold
        } else {
          if(federation=='ENG') {
            if(silver=='G' || silver=='P' || silver=='K' || silver=='L') {
              $("#edit-play-fee").get(0).value = 0;
            } else {
              if( silver=='J' ) {
                $("#edit-play-fee").get(0).value = section.junior_charge;
                totalPrice+=parseFloat(section.junior_charge);
              } else {
                $("#edit-play-fee").get(0).value = gold_charge;
                totalPrice+=parseFloat(gold_charge);
              }
            }
          } else {
            $("#edit-play-fee").get(0).value = 0;
            if(silver!='G' && silver!='P' && silver!='K' && silver!='L') {
              fide_fee=fide_charge;
            }
          }
        }
      }
    }
    // Set entry fee
    sid=$("#edit-sid").get(0).value;
    section = congress_sections[sid];
    var entry_fee=section.fee;
    totalPrice+=parseFloat(entry_fee);
    $("#edit-entry-fee").get(0).value = entry_fee;
  }
  // Set the extra discount
  extra_fee=0;
  if(extra_discount>0) {
    extra=$("#edit-extra").get(0).value;
    if(extra=='Y') {
      extra_fee=-extra_discount;
    }
    $("#edit-extra-fee").get(0).value = extra_fee;
    totalPrice+=extra_fee;
  }
  // Add in the late entry fee
  totalPrice+=late_entry_fee;
  // Set Late entry fee field
  $("#edit-late-fee").get(0).value = late_entry_fee;
  // Set FIDE charge
  $("#edit-federation-fee").get(0).value = fide_fee;
  totalPrice+=fide_fee;
  // Set discount based on DOB
  dobFee=0;
  if( $("#edit-age-option").length ) {
    age_option=$("#edit-age-option").get(0).value;
    if(age_option=='J') {
      dobFee = junior_discount;
    }
    if(age_option=='S') {
      dobFee = vetran_discount;
    }
    $("#edit-dob-fee").get(0).value = dobFee;
  }
  totalPrice+=dobFee;
  // Set total price field
  $("#edit-total-price").get(0).value = totalPrice;
}
  // Code to run after page load
  Drupal.ajax.prototype.commands.insertGrade = function(ajax, response, status)
    {
        // This insertGrade is an Ajax command that drupal sends back.
        // It puts the ecfcode selected from the modal into the form.
        $("#edit-ecfcode").val(response.ecfcode);
        $("#edit-fidecode").val(response.fidecode);
        $("#edit-grade").val(response.grade);
        $("#edit-rgrade").val(response.rgrade);
        $("#edit-ecfmemb").val(response.memb);
        $("#edit-club").val(String(response.club));
    };
  // set initial total price
  setSection();
  // set function to be called when each selector is changed:
  // -extra discount
  if(extra_discount>0) {
    $("#edit-extra").get(0).onchange = addPrice;
  }
  if(multiple_sections=='Y') {
    $("[id^=edit-sid]").each(function(i) {
      this.onchange = setSection;
    });
  } else {
    $("#edit-sid").get(0).onchange = setSection;
  }
  $("#edit-silver").get(0).onchange = addPrice;
  $("#edit-federation").get(0).onchange = addPrice;
  if( $("#edit-age-option").length ) {
    $("#edit-age-option").get(0).onchange = addPrice;
  }
  $("#entry-clear").get(0).onclick = clearGrade;
  $("#donation-add").get(0).onclick = addDonation;
  $("#donation-sub").get(0).onclick = subDonation;
  // Add handler to set zero or half point bye to bye selectors
  $("[id^=edit-bye] .form-select").each(function(i) {
    this.onchange = changeByePoints;
  });

});
})(jQuery);
