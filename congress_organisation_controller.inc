<?php
/**
 * @file
 * Congress Organisation controller
 */

class CongressOrganisationController extends DrupalDefaultEntityController {
  public function create($rid) {
    return (object) array(
      'rid' => $rid,
      'name' => '',
      'logo' => '',
      'is_new' => TRUE, 
    );
  }

  public function load($ids = array(), $conditions = array()) {
    $objects = parent::load($ids, $conditions);
    return $objects;
  }

  public function save($organisation) {
    $transaction = db_transaction();

    try {
      if(isset($organisation->is_new)) {
        drupal_write_record('congress_organisation', $organisation);
        $op = 'insert';
      } else {
        drupal_write_record('congress_organisation', $organisation, 'rid');
        $op = 'update';
      }
      // Save fields
      $function = 'field_attach_' . $op;
      $function('congress_organisation', $organisation);

      module_invoke_all('entity_' . $op, $organisation, 'congresss_organisation');
      unset($organisation->is_new);

      db_ignore_slave();

      return $organisation;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('congress', $e, NULL, WATCHDOG_ERROR);
      return FALSE;
    }
  }

  public function delete($rids) {
    if(!empty($rids)) {
      $entries = $this->load($rids, array());
      $transaction = db_transaction();

      try {
        db_delete('congress_organisation')
          ->condition('rid', $rids, 'IN')
          ->execute();

        foreach($entries as $rid => $organisation) {
          field_attach_delete('congress_organisation', $organisation);
        }

        db_ignore_slave();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('congress', $e, NULL, WATCHDOG_ERROR);
        return FALSE;
      }

      module_invoke_all('entity_delete', $organisation, 'congress_organisation');

      cache_clear_all();
      $this->resetCache();
    }
    
    return TRUE;
  }
}

