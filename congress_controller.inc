<?php
/**
 * @file
 * Congress controller
 */

class CongressController extends DrupalDefaultEntityController {
  public function create() {
    return (object) array(
      'cid' => '',
      'name' => '',
      'pmid' => '',
      'rid' => 1,
      'email' => '',
      'date' => null,
      'late_date' => null,
      'late_fee' => 0,
      'late_date2' => null,
      'late_fee2' => 0,
      'junior_discount' => 0,
      'vetran_discount' => 0,
      'ask_dob' => 'N',
      'dob_prompt' => 'Juniors',
      'ask_female' => 'N',
      'entry_text' => '',
      'country' => 'ENG',
      'status' => 'A',
      'max_entrants' => 0,
      'entry_closed_days' => 0,
      'logo' => '',
      'entrants_text' => '',
      'multiple_discount' => 0,
      'multiple_sections' => FALSE,
      'age_date' => null,
      'extra_discount' => 0,
      'extra_discount_text' => '',
      'member_discount' => 'Y',
      'platform' => 'O',
      'extra_question' => '',
      'style' => 'O',
      'sections' => array(),
    );
  }

  public function section($cid) {
    return (object) array(
      'sid' => '',
      'cid' => $cid,
      'name' => '',
      'rating_type' => 'E',
      'entry_fee' => '0.0',
      'grade_limit' => 0,
      'speed' => 'B',
      'rounds' => 5,
      'byes' => 1,
      'age' => 0,
      'grp' => 'AA',
    );
  }

  public function load($ids = array(), $conditions = array()) {
    $objects = parent::load($ids, $conditions);
    foreach($objects as $object) {
      $object->sections = $this->load_sections($object->cid);
    }
    return $objects;
  }

  private function save_sections($congress) {
    foreach($congress->sections as $section) {
      $is_new = empty($section->sid);
      if($is_new) {
        drupal_write_record('congress_section', $section);
      } else {
        drupal_write_record('congress_section', $section, 'sid');
      }
    }
  }

  private function load_sections($cid) {
    $sections = array();
    $query = db_select('congress_section', 'cs');
    $query->condition('cs.cid', $cid);
    $query->fields('cs', array('sid', 'name', 'entry_fee', 'grade_limit', 'rating_type', 'speed', 'rounds', 'byes', 'age', 'grp'));
    //print out query
    //dsm((string)$query);
    $result = $query->execute();
    // Transfer from object $row to array rows
    foreach ( $result as $row) {
      $sections[$row->sid] = (object)$row;
    }

    return $sections;
  }

  public function save($congress) {
    $transaction = db_transaction();

    try {
      $congress->is_new = empty($congress->cid);
      if($congress->is_new) {
        drupal_write_record('congress', $congress);
        $op = 'insert';
      } else {
        drupal_write_record('congress', $congress, 'cid');
        $op = 'update';
      }
      // Save fields
      $function = 'field_attach_' . $op;
      $function('congress', $congress);

      module_invoke_all('entity_' . $op, $congress, 'congress');
      unset($congress->is_new);

      // Save sections
      $this->save_sections($congress);

      db_ignore_slave();

      return $congress;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('congress', $e, NULL, WATCHDOG_ERROR);
      return FALSE;
    }
  }

  public function delete($cids) {
    if(!empty($cids)) {
      $congresses = $this->load($cids, array());
      $transaction = db_transaction();

      try {
        db_delete('congress')
          ->condition('cid', $cids, 'IN')
          ->execute();

        foreach($congresses as $cid => $congress) {
          field_attach_delete('congress', $congress);
        }

        db_ignore_slave();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('congress', $e, NULL, WATCHDOG_ERROR);
        return FALSE;
      }

      module_invoke_all('entity_delete', $congress, 'congress');

      cache_clear_all();
      $this->resetCache();
    }
    
    return TRUE;
  }
}

