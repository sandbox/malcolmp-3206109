<?php
/**
* @file
* Functions for updating a congress.
*/

function congress_contact_page($congress) {

  $title = $congress->name;
  drupal_set_title(' ');
  return drupal_get_form('congress_contact_form', $congress);
}
 
/**
*  congress contact form.
*/
function congress_contact_form($form, &$form_state, $congress) {

  $form['#congress'] = $congress;
  $form_state['congress'] = $congress;

  $form['email'] = array(
   '#type'  => 'textfield',
   '#size'  => 30,
   '#maxlength'  => 30,
   '#title'  => t('Email address'),
   '#required' => true,
   '#weight' => 5,
  );

  $form['name'] = array(
   '#type'  => 'textfield',
   '#size'  => 30,
   '#maxlength'  => 30,
   '#title'  => t('Name'),
   '#required' => true,
   '#weight' => 6,
  );

  $form['message'] = array(
   '#type'  => 'textarea',
   '#rows'  => 5,
   '#cols'  => 50,
   '#title'  => t('Enter message for congress organiser'),
   '#required' => true,
   '#weight' => 10,
  );

  $form['submit'] = array(
     '#value' => t('Send Message'),
     '#type'  => 'submit',
     '#weight' => 15,
  );

  return $form;
}

/**
*  Action after the form is submitted.
*/
function congress_contact_form_submit($form, &$form_state) {
  $op = $form_state['values']['op'];
  $congress = $form_state['congress'];
  $message = $form_state['values']['message'];
  $email = $form_state['values']['email'];
  $name = $form_state['values']['name'];

  // Apply filter - only for html?
  $message = check_markup($message);

  $subject = 'Message from contact form for: ' . $congress->name;

  // take the first of comma seperated list.
  // TODO why not send to everyone ?
  $mailto = $congress->email;
  // email data
//$mailfrom = 'donotreply@congress.org.uk';
  // This probably gets overitten anyway
  $mailfrom = $email;
  $mail_link = '<a href="mailto:' . $email . '?subject=Re ' . $congress->name . '">' . "$email </a>";
  $full_message = "Message from $name . Press the reply button in your email program to reply or use this link $mail_link <p><p>$message";
  $params = array(
            'data' => $full_message,
            'subject' => $subject,
            'Reply-To' => $email,
            );
  watchdog('congress','Send contact mail to ' . $mailto . ' from ' . $mailfrom );
  // Send the contact email
  drupal_mail('congress',         // module implementing hook_mail
              'contact',          // key defining this mail
              $mailto,            // email addresses to send to
              language_default(),
              $params,            // params to pass to hook_mail
              $mailfrom,          // from email address
              TRUE);              // Send the mail

  drupal_set_message("Message sent to the congress organiser");
  drupal_goto('congress/' . $congress->cid . '/home');
}
