<?php
/**
* @file
* Functions for payment by bank transfer.
*/

function congress_entry_pay_page($congress, $order) {
  $price = $order->price;
  $bank_text = $congress->bank_text;

  $text = '<h2>Payment By Bank Transfer </h2>';
  $text .= "The organiser has been notified of your entry. The price is:<p> £ $price <p><b>You must now pay</b> according to the organisers instructions below: <p><p>";
  $text .= $bank_text;
  $text .= '<p>The entry will appear in the entrants list after the organiser has approved it. Any questions contact the congress organiser ';
  $link = l('using this link', "congress/$congress->cid/home");
  $text .= $link;
  $content = array(
    '#type' => 'markup',
    '#markup' => $text,
  );
    
  return $content;
}
