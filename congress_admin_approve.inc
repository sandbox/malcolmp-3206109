<?php
/**
* @file
* Functions for approving free or bank transfer entries.
*/

function congress_admin_approve($congress) {
  return drupal_get_form('congress_approve_form', $congress);
}

/**
*  Form for approving congress entries.
*/
function congress_approve_form($form, &$form_state, $congress) {

  // Get orders needing approval
  $orders = congress_entries_to_approve($congress);

  // Store in form
  $form_state['congress'] = $congress;

  // Intro text
  $form = [];
  $form['text'] = array(
    '#type'  => 'markup',
    '#markup' => 'Entries with payment by bank transfer or free entries are shown below. Pressing the approve button will change the entry status so that it appears in the entrants table and a confirmation email will be sent to the entrant',
  );

  // Entries table header
  $weight = 0;
  $form['entries'] = array(
    '#type'  => 'markup',
    '#prefix' => '<table border><tr><th>Approve</th><th>Price</th><th>Section</th><th>Name</th><th>Status</th></tr>',
    '#weight' => $weight,
    '#suffix' => '</table>',
  );

  // For each order ...
  foreach($orders as $oid => $entries) {
    $first = TRUE;
    $rowspan = count($entries);

    // For each entry ...
    foreach($entries as $eid => $entry) {
      // Add a row for it to the table
      $name = $entry['name'];
      $section = $entry['section'];
      $price = $entry['price'];
      $status = $entry['status'];
      $row = "<td>$section</td><td>$name</td><td>$status</td></tr>";
      $weight++;
      // If its the first in the order, add the approve button and price
      if($first) {
        $first = FALSE;
        // Approve button
        $form['entries']['approve'.$eid] = array(
          '#weight' => $weight,
          '#value' => t('Approve'),
          '#attributes' => array(
            'class' => array('approve-button-'.$oid),
          ),
          '#type'  => 'submit',
          '#name'  => 'approve'.$oid,
//        '#submit'  => array('congress_enter_form_edit'),
          '#prefix' => '<tr><td rowspan="' . $rowspan . '">',
          '#suffix' => '</td><td rowspan="' . $rowspan . '">' . $price . '</td>' . $row,
        );
      } else {
        $form['entries']['approve'.$eid] = array(
          '#type'  => 'markup',
          '#markup'  => '<tr>' . $row,
          '#weight' => $weight,
        );
      }
    } // End each entry
  } // End each order
    
  return $form;
}

/**
*  Action after the form is submitted.
*  Update congress record
*/
function congress_approve_form_submit($form, &$form_state) {
  // Check access
  $congress = $form_state['congress'];
  if(!congress_check_access($congress) ) {
    drupal_set_message(t('Access Denied'));
    drupal_goto('congress/' . $congress->cid);
    return;
  }

  $name = $form_state['triggering_element']['#name'];
  $oid = substr($name, 7);
  $message = "Order $oid approved for: ";
  $cid = 0;
  $entries = congress_order_entries($oid);
  // For each entry on the order, set status
  foreach($entries as $eid) {
    $entry = congress_entry_load($eid);
    if( $entry->status == 'OTHER_SENT' ) {
      $entry->status = 'OTHER_PAID';
    } else {
      $entry->status = 'ADMIN_FREE';
    }
    congress_entry_save($entry);
    // Create user and default mail subs if they don't exist
    if(isset($entry->email) && strlen($entry->email)>3) {
      congress_update_user($entry);
    }

    // add entrants to the message
    $message .= $entry->first_name . ' ' . $entry->last_name . ', ';
    $cid = $entry->cid;
  }
  drupal_set_message($message);
  $order = congress_order_load($oid);
  congress_order_send_emails($order, 'approve');
  drupal_goto('congress/' . $cid . '/admin');
}

function congress_entries_to_approve($congress) {
  # Players Query
  $query = db_select('congress_entry', 'ce');
  // Filter by congress
  $query->condition('ce.cid', $congress->cid);
  // status ( could be SUBMITTED if paypal free entry? )
  $states = ['OTHER_SENT', 'SUBMITTED'];
  $query->condition('ce.status', $states, 'IN');
  $query->fields('ce', array('eid', 'first_name', 'last_name', 'oid', 'status'));
  $query->innerJoin('congress_entry_sections', 'ces', "ces.eid = ce.eid");
  $query->innerJoin('congress_section', 'cs', "cs.sid = ces.sid");
  $query->addField('cs', 'name', 'section');
  $query->innerJoin('congress_order', 'co', 'co.oid = ce.oid');
  $query->addField('co', 'price', 'price');
  $result = $query->execute();

  $orders = [];
  // Get the rows from the query
  foreach ( $result as $row) {
    // Only include bank transfer (OTHER_SENT) or free entries
    if( $row->status == 'OTHER_SENT' || $row->price == 0) {
      if( !isset($orders[$row->oid]) ) {
        $orders[$row->oid] = [];
      }
      $entry = [
        'name' => $row->first_name . ' ' . $row->last_name,
        'status' => $row->status,
        'price' => $row->price,
        'section' => $row->section,
      ];
      $eid = $row->eid;
      $orders[$row->oid][$eid] = $entry ;
    }
  }
  return $orders;
}
