<?php
// $Id$
/**
 * @file
 * Functions for League fixture options.
 */

function congress_admin_list() {

  $content['header']['#markup'] = l('Add Congress', "congress/add");
  $content['header']['#type'] = 'markup';

  $table = congress_list_table();
  $rows = $table['data'];     // Actual table data
  $header = $table['header']; // Headers
  $content['list']['#rows'] = $rows;
  $content['list']['#header'] = $header;
  $content['list']['#theme'] = 'table';
  $content['pager']['#markup'] = theme('pager');
  return $content;
}
