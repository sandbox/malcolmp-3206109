<?php
/**
* @file
* Chess Congress Module - Install Code
*/

/**
 * Implementation of hook_install().
 */

function congress_install() {
}

/**
 * Implementation of hook_uninstall().
 */

function congress_uninstall() {
}

/**
 * Implementation of hook_schema().
 */
function congress_schema() {

  $schema['congress'] = array(
    'description' => 'Chess Congress',
    'fields' => array(
      'cid' => array(
        'description' => 'Congress ID',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'Name of the congress',
        'type' => 'varchar',
        'length' => '30',
        'not null' => TRUE,
      ),
      'pmid' => array(
        'description' => 'Payment ID',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'rid' => array(
        'description' => 'Access Role',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'email' => array(
        'description' => 'Email Recipients',
        'type' => 'varchar',
        'length' => '200',
        'not null' => TRUE,
      ),
      'date' => array(
        'description' => 'Date of congress',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'late_date' => array(
        'description' => 'Late Entry Date',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'late_fee' => array(
        'description' => 'Late Entry Fee',
        'type' => 'varchar',
        'length' => '8',
        'not null' => TRUE,
      ),
      'late_date2' => array(
        'description' => 'Late Entry Date 2',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'late_fee2' => array(
        'description' => 'Late Entry Fee 2',
        'type' => 'varchar',
        'length' => '8',
        'not null' => TRUE,
      ),
      'junior_discount' => array(
        'description' => 'Junior discount or zero',
        'type' => 'varchar',
        'length' => '8',
        'not null' => TRUE,
      ),
      'vetran_discount' => array(
        'description' => 'Vetran discount or zero',
        'type' => 'varchar',
        'length' => '8',
        'not null' => TRUE,
      ),
      'ask_dob' => array(
        'description' => 'Should we ask for DOB?',
        'type' => 'char',
        'length' => '1',
        'not null' => TRUE,
      ),
      'dob_prompt' => array(
        'description' => 'Explanatory text next to date of birth',
        'type' => 'varchar',
        'length' => '40',
        'not null' => TRUE,
      ),
      'ask_female' => array(
        'description' => 'Should we ask if female ?',
        'type' => 'char',
        'length' => '1',
        'not null' => TRUE,
      ),
      'entry_text' => array(
        'description' => 'Explanatory text shown to user',
        'type' => 'text',
        'length' => '400',
        'not null' => TRUE,
      ),
      'country' => array(
        'description' => 'Country hosting the congress',
        'type' => 'char',
        'length' => '3',
        'not null' => TRUE,
      ),
      'status' => array(
        'description' => 'Status of congress A=active',
        'type' => 'char',
        'length' => '1',
        'not null' => TRUE,
      ),
      'max_entrants' => array(
        'description' => 'Maximum number of entries',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'logo' => array(
        'description' => 'Image file for congress',
        'type' => 'varchar',
        'length' => '30',
        'not null' => TRUE,
      ),
      'entrants_text' => array(
        'description' => 'Text before entrants list',
        'type' => 'text',
        'length' => '100',
        'not null' => FALSE,
      ),
      'entry_closed_days' => array(
        'description' => 'Days before congress when online entry closes',
        'type' => 'int',
        'unsigned' => FALSE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'multiple_sections' => array(
        'description' => 'Is entering more than one section allowed?',
        'type' => 'char',
        'length' => '1',
        'not null' => TRUE,
        'default' => '0',
      ),
      'multiple_discount' => array(
        'description' => 'Multiple discount or zero',
        'type' => 'varchar',
        'length' => '8',
        'not null' => TRUE,
        'default' => 0,
      ),
      'extra_discount' => array(
        'description' => 'Extra discount or zero',
        'type' => 'varchar',
        'length' => '8',
        'not null' => TRUE,
        'default' => 0,
      ),
      'extra_discount_text' => array(
        'description' => 'Extra discount text',
        'type' => 'varchar',
        'length' => '30',
        'not null' => FALSE,
      ),
      'age_date' => array(
        'description' => 'Date for determining who is junior / vetran',
        'type' => 'int',
        'not null' => FALSE,
      ),
      'member_discount' => array(
        'description' => 'Give members discounts',
        'type' => 'char',
        'length' => '1',
        'not null' => TRUE,
        'default' => 'Y',
      ),
      'platform' => array(
        'description' => 'Platform eg otb or chess.com',
        'type' => 'char',
        'length' => '1',
        'not null' => TRUE,
        'default' => 'O',
      ),
      'extra_question' => array(
        'description' => 'Extra Question',
        'type' => 'varchar',
        'length' => '30',
        'not null' => FALSE,
      ),
      'style' => array(
        'description' => 'Style of entry form',
        'type' => 'char',
        'length' => '1',
        'not null' => TRUE,
        'default' => 'O',
      ),
      'bank_option' => array(
        'description' => 'Pay by bank transfer',
        'type' => 'char',
        'length' => '1',
        'not null' => TRUE,
        'default' => 'N',
      ),
      'bank_text' => array(
        'description' => 'Bank payment details',
        'type' => 'varchar',
        'length' => '64',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('cid'),
  );

  $schema['congress_section'] = array(
    'description' => 'Chess Congress Section',
    'fields' => array(
      'sid' => array(
        'description' => 'Section ID',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'cid' => array(
        'description' => 'Congress ID',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'Name of the section',
        'type' => 'varchar',
        'length' => '30',
        'not null' => TRUE,
      ),
      // This may contain pence, so store as char
      'entry_fee' => array(
        'description' => 'Entry Fee for the section',
        'type' => 'varchar',
        'length' => '8',
        'not null' => TRUE,
      ),
      'grade_limit' => array(
        'description' => 'Grading Limit for the section',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'rating_type' => array(
        'description' => 'ECF Graded or FIDE rated (E, F, U)',
        'type' => 'char',
        'length' => '1',
        'not null' => TRUE,
      ),
      'speed' => array(
        'description' => 'Standard, Rapid, or Blitz',
        'type' => 'char',
        'length' => '1',
        'not null' => TRUE,
      ),
      'rounds' => array(
        'description' => 'Number of rounds',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'byes' => array(
        'description' => 'Number of byes allowed',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'age' => array(
        'description' => 'Age Limit',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'grp' => array(
        'description' => 'Section Group',
        'type' => 'char',
        'length' => '2',
        'not null' => TRUE,
        'default' => 'AA',
      ),
    ),
    'primary key' => array('sid','cid'),
  );

  $schema['congress_entry'] = array(
    'description' => 'Chess Congress Entry',
    'fields' => array(
      'eid' => array(
        'description' => 'Entry ID',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'cid' => array(
        'description' => 'Congress ID',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'oid' => array(
        'description' => 'Order ID',
        'type' => 'int',
        'default' => 0,
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
      'first_name' => array(
        'description' => 'First Name',
        'type' => 'varchar',
        'length' => '30',
        'not null' => TRUE,
      ),
      'last_name' => array(
        'description' => 'Last Name',
        'type' => 'varchar',
        'length' => '30',
        'not null' => TRUE,
      ),
      'email' => array(
        'description' => 'Email address',
        'type' => 'varchar',
        'length' => '50',
        'not null' => TRUE,
      ),
      'address1' => array(
        'description' => 'Address First line',
        'type' => 'varchar',
        'length' => '30',
        'not null' => TRUE,
      ),
      'address2' => array(
        'description' => 'Address Second line',
        'type' => 'varchar',
        'length' => '30',
        'not null' => TRUE,
      ),
      'city' => array(
        'description' => 'City',
        'type' => 'varchar',
        'length' => '30',
        'not null' => TRUE,
      ),
      'postcode' => array(
        'description' => 'Post Code',
        'type' => 'varchar',
        'length' => '10',
        'not null' => TRUE,
      ),
      'telephone' => array(
        'description' => 'Telephone Number',
        'type' => 'varchar',
        'length' => '20',
        'not null' => TRUE,
      ),
      'donation' => array(
        'description' => 'Donation',
        'type' => 'varchar',
        'length' => '8',
        'not null' => TRUE,
      ),
      'silver' => array(
        'description' => 'ECF Silver member',
        'type' => 'char',
        'length' => '1',
        'not null' => TRUE,
      ),
      'late' => array(
        'description' => 'Late Entry',
        'type' => 'varchar',
        'length' => '8',
        'not null' => TRUE,
      ),
      'sex' => array(
        'description' => 'Sex',
        'type' => 'char',
        'length' => '1',
        'not null' => FALSE,
      ),
        'dob' => array(
        'description' => 'Date of birth as unix timestamp',
        'type' => 'int',
        'not null' => FALSE,
      ),
        'ecfcode' => array(
        'description' => 'ECF Grading Code',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
        'ecfmemb' => array(
        'description' => 'ECF Membership Number',
        'type' => 'char',
        'length' => '8',
        'not null' => FALSE,
      ),
      'fidecode' => array(
        'description' => 'FIDE Rating Code',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
      'price' => array(
        'description' => 'Total Payed',
        'type' => 'varchar',
        'length' => '8',
        'not null' => FALSE,
      ),
      'grade' => array(
        'description' => 'Grade',
        'type' => 'varchar',
        'length' => '4',
        'not null' => FALSE,
      ),
      'grade_details' => array(
        'description' => 'Grade details for ungraded',
        'type' => 'varchar',
        'length' => '40',
        'not null' => FALSE,
      ),
      'club' => array(
        'description' => 'Club',
        'type' => 'char',
        'length' => '8',
        'not null' => FALSE,
      ),
      'status' => array(
        'description' => 'Status',
        'type' => 'varchar',
        'length' => '20',
        'not null' => TRUE,
      ),
      'session' => array(
        'description' => 'Session ID',
        'type' => 'varchar',
        'length' => '128',
        'not null' => FALSE,
      ),
      'federation' => array(
        'description' => 'FIDE Federation',
        'type' => 'char',
        'length' => '3',
        'default' => 'XXX',
        'not null' => TRUE,
      ),
        'updated' => array(
        'description' => 'Update timestamp',
        'type' => 'int',
        'not null' => FALSE,
      ),
        'extra' => array(
        'description' => 'Extra Fee',
        'type' => 'varchar',
        'length' => '8',
        'not null' => TRUE,
        'default' => '0',
      ),
      'age_option' => array(
        'description' => 'Junior or Senior',
        'type' => 'char',
        'length' => '1',
        'not null' => TRUE,
        'default' => 'N',
      ),
      'onlineid' => array(
        'description' => 'ID in chess.com or lichess',
        'type' => 'varchar',
        'length' => '30',
        'not null' => TRUE,
      ),
      'extra_reply' => array(
        'description' => 'Extra Reply',
        'type' => 'varchar',
        'length' => '64',
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('eid'),
    'indexes' => array(
      'cid' => array('cid'),
    ),

  );

  $schema['congress_order'] = array(
    'description' => 'Chess Congress Order',
    'fields' => array(
      'oid' => array(
        'description' => 'Order ID',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'price' => array(
        'description' => 'Total Payed',
        'type' => 'varchar',
        'length' => '8',
        'not null' => FALSE,
      ),
      'status' => array(
        'description' => 'Order Status',
        'type' => 'varchar',
        'length' => '20',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('oid'),
  );

  $schema['congress_user_email'] = array(
    'description' => 'Chess Congress Email Preferences',
    'fields' => array(
      'uid' => array(
        'description' => 'User ID',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'rid' => array(
        'description' => 'Role ID',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'subscription' => array(
        'description' => 'Subscription Flag',
        'type' => 'int',
        'unsigned' => TRUE,
        'default' => 0,
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('uid','rid'),
  );

  $schema['congress_user'] = array(
    'description' => 'Chess Congress User Tab data',
    'fields' => array(
      'uid' => array(
        'description' => 'User ID',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'first_name' => array(
        'description' => 'First Name',
        'type' => 'varchar',
        'length' => '30',
        'not null' => TRUE,
      ),
      'last_name' => array(
        'description' => 'Last Name',
        'type' => 'varchar',
        'length' => '30',
        'not null' => TRUE,
      ),
        'ecfcode' => array(
        'description' => 'ECF Grading Code',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
      'fidecode' => array(
        'description' => 'FIDE Rating Code',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
      ),
      'address1' => array(
        'description' => 'Address First line',
        'type' => 'varchar',
        'length' => '30',
        'not null' => TRUE,
      ),
      'address2' => array(
        'description' => 'Address Second line',
        'type' => 'varchar',
        'length' => '30',
        'not null' => TRUE,
      ),
      'city' => array(
        'description' => 'City',
        'type' => 'varchar',
        'length' => '30',
        'not null' => TRUE,
      ),
      'postcode' => array(
        'description' => 'Post Code',
        'type' => 'varchar',
        'length' => '10',
        'not null' => TRUE,
      ),
      'telephone' => array(
        'description' => 'Telephone Number',
        'type' => 'varchar',
        'length' => '20',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('uid'),
  );

  $schema['congress_entry_sections'] = array(
    'description' => 'Chess Congress Entry Sections',
    'fields' => array(
      'eid' => array(
        'description' => 'Entry ID',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'sid' => array(
        'description' => 'Section ID',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'bye1' => array(
        'description' => 'Bye',
        'type' => 'varchar',
        'length' => '2',
        'not null' => TRUE,
      ),
      'bye2' => array(
        'description' => 'Bye',
        'type' => 'varchar',
        'length' => '2',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('eid','sid'),
  );

  $schema['congress_organisation'] = array(
    'description' => 'Chess Congress Organisation',
    'fields' => array(
      'rid' => array(
        'description' => 'Organisation Role ID',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'name' => array(
        'description' => 'Name of the organisation',
        'type' => 'varchar',
        'length' => '30',
        'not null' => TRUE,
      ),
      'logo' => array(
        'description' => 'Image file for organisation',
        'type' => 'varchar',
        'length' => '30',
        'not null' => TRUE,
      ),
    ),
    'primary key' => array('rid'),
  );

  return $schema;
}

/**
 * Add extra_question field to congress table.
 */
function congress_update_7001() {
  $extra_question = array(
    'description' => 'Extra Question',
    'type' => 'varchar',
    'length' => '30',
    'not null' => FALSE,
  );
  db_add_field( 'congress', 'extra_question', $extra_question);
}
/**
 * Add extra_reply field to congress_entry table.
 */
function congress_update_7002() {
  $extra_reply = array(
    'description' => 'Extra Reply',
    'type' => 'varchar',
    'length' => '16',
    'not null' => FALSE,
  );
  db_add_field( 'congress_entry', 'extra_reply', $extra_reply);
}
/**
 * Add style field to congress table.
 */
function congress_update_7003() {
  $style = array(
    'description' => 'Style of entry form',
    'type' => 'char',
    'length' => '1',
    'not null' => TRUE,
    'default' => 'O',
  );
  db_add_field( 'congress', 'style', $style);
}
/**
 * increase size of extra_reply field in congress_entry table.
 */
function congress_update_7004() {
  $extra_reply = array(
    'description' => 'Extra Reply',
    'type' => 'varchar',
    'length' => '64',
    'not null' => FALSE,
  );
  db_change_field( 'congress_entry', 'extra_reply', 'extra_reply', $extra_reply);
}
/**
 * Add options for payment by bank transfer.
 */
function congress_update_7005() {
  $bank_option = array(
    'description' => 'Pay by bank transfer',
    'type' => 'char',
    'length' => '1',
    'not null' => TRUE,
    'default' => 'N',
  );
  db_add_field( 'congress', 'bank_option', $bank_option);
  $bank_text = array(
    'description' => 'Bank payment details',
    'type' => 'varchar',
    'length' => '64',
    'not null' => FALSE,
  );
  db_add_field( 'congress', 'bank_text', $bank_text);
}
/**
 * Increase size of bank_text field.
 */
function congress_update_7006() {
  $bank_text = array(
    'description' => 'Bank payment details',
    'type' => 'varchar',
    'length' => '128',
    'not null' => FALSE,
  );
  db_change_field( 'congress', 'bank_text', 'bank_text', $bank_text);
}
/**
 * Ensure fidecodes are valid ints
 */
function congress_update_7007() {
  $sql = "UPDATE congress_entry SET fidecode= '0' WHERE fidecode NOT REGEXP '^-?[0-9]+$'";
  $result = db_query($sql);
  $sql = "UPDATE congress_user SET fidecode= '0' WHERE fidecode NOT REGEXP '^-?[0-9]+$'";
  $result = db_query($sql);
  $sql = "UPDATE congress_entry SET fidecode='0' WHERE trim(coalesce(fidecode, '')) = ''";
  $result = db_query($sql);
  $sql = "UPDATE congress_user SET fidecode='0' WHERE trim(coalesce(fidecode, '')) = ''";
  $result = db_query($sql);
}
/**
 * Change fidecode to int on congress_user and congress_entry tables
 */
function congress_update_7008() {
  $fidecode = array(
    'description' => 'FIDE Rating Code',
    'type' => 'int',
    'unsigned' => TRUE,
    'not null' => FALSE,
  );
  db_change_field( 'congress_entry', 'fidecode', 'fidecode', $fidecode);
  db_change_field( 'congress_user', 'fidecode', 'fidecode', $fidecode);
}
/**
 * Change club to char 8 to match rating_list_player_clubs. Add index.
 */
function congress_update_7009() {
  $club = array(
    'description' => 'Club',
    'type' => 'char',
    'length' => '8',
    'not null' => FALSE,
  );
  db_change_field( 'congress_entry', 'club', 'club', $club);
  db_add_index('congress_entry', 'cid', array('cid'));
}

