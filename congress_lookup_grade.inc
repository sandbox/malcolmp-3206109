<?php
/**
* @file
* Functions for grade lookup.
*/

function congress_lookup_grade($js=NULL) {
  $name = '';
  if(isset($_POST['last_name']) ) {
    $name = $_POST['last_name'];
  }
  $first_name = '';
  if(isset($_POST['first_name']) ) {
    $first_name = $_POST['first_name'];
  }
  $initial =  '';
  if(strlen($name) > 1 && strlen($first_name) > 0) {
    $initial = substr($first_name, 0, 1);
  }
  $country = '';
  if(isset($_POST['country']) ) {
    $country = $_POST['country'];
  }
  $federation = $country;
  if(isset($_POST['federation']) ) {
    $federation = $_POST['federation'];
  }
  if ($js) {
    ctools_include('ajax');
    ctools_include('modal');

    $form_state = array(
      'ajax' => TRUE,
      'title' => t('Pick yourself from the grading list'),
      'congress_name' => $name,
      'congress_initial' => $initial,
      'congress_country' => $country,
      'congress_federation' => $federation,
    );

    // Use ctools to generate ajax instructions for the browser to create
    // a form in a modal popup.
    $output = ctools_modal_form_wrapper('congress_grade_lookup_form', $form_state);

    // If the form has been submitted, there may be additional instructions
    // such as dismissing the modal popup.
    if (!empty($form_state['ajax_commands'])) {
      $output = $form_state['ajax_commands'];
    }

    // Return the ajax instructions to the browser via ajax_render().
    print ajax_render($output);
    drupal_exit();
  }
  else {
    return drupal_get_form('congress_grade_lookup_form', $name, $country);
  }
}


/**
*  Form for grade lookup.
*/
function congress_grade_lookup_form($form, &$form_state ) {

  $name = $form_state['congress_name'];
  $initial = $form_state['congress_initial'];
  $country = $form_state['congress_country'];
  $federation = $form_state['congress_federation'];
  if (strlen($name) < 2) {
    $get = ' ';
    if(isset($_POST['last_name']) ) {
      $get = $_POST['last_name'];
    }
    $debug = print_r($_POST, true);
    $debug = $get;
    $link_table = "<h2>ERROR: You must specify surname to lookup your grade $debug</h2>";
  } else {
    $link_table = congress_grade_select_list($name,$initial,$country,$federation);
  }

  $form['players'] = array(
     '#markup' => "<div id=grade-select-table>$link_table</div>",
     '#type'  => 'markup',
  );

  $form['cancel'] = array(
     '#value' => t('Cancel'),
     '#type'  => 'submit',
     '#weight' => 5,
  );

  return $form;
}

function congress_grade_lookup_form_submit($form, &$form_state) {
  // Tell the browser to close the modal.
  $form_state['ajax_commands'][] = ctools_modal_command_dismiss();
}

