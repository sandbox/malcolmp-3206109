<?php
/**
* @file
* Functions for updating a congress.
*/

function congress_mass_email_page($rid) {
  $organisation = congress_organisation_load($rid);
  $title = "Congress Mass email for: $organisation->name";
//$mail_limit = 1000;
  // No limit
  $mail_limit = 0;

  $content = user_tools_mass_email_page($title, 'Rating ID', 'congress', 'mass', $mail_limit, $rid );

  return $content;
}
 
function congress_mail_from_congress($congress) {
  drupal_set_title(' ');
  $rid = $congress->rid;
  return congress_mass_email_page($rid);
}
