<?php
/**
* @file
* Functions for generating XL list of players
*/

// Library for writing XL files
$exlib = libraries_get_path('vendor');
require_once "$exlib/autoload.php";
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

function congress_admin_xl($congress, $type='S', $filter='ALL') {
  if($type == 'S' || $type == 'M' || $type == 'E') {
    congress_admin_xl_types($congress,$type,$filter);
  } else {
    congress_admin_txt_types($congress,$type,$filter);
  }
}

function congress_admin_xl_types($congress,$type,$filter) {

  // Create new PHPExcel object
  $objPHPExcel = new Spreadsheet();

  // Create a workbook
  $objPHPExcel->setActiveSheetIndex(0);

  $first = TRUE;
  foreach($congress->sections as $section) {
    if($first) {
      $sheet = $objPHPExcel->getActiveSheet();
      $first = FALSE;
    } else {
      $sheet = $objPHPExcel->createSheet();
    }
    $title = congress_string2filename($section->name);
    $sheet->setTitle($title);
    $entrants = array();
    if($type == 'E') {
      $entrants = congress_xl_entries_entry($congress,$section,$filter);
      congress_admin_xl_section_entry($sheet, $section, $entrants);
    } else {
      $entrants = congress_xl_entries($congress,$section,'rating',$filter);
      congress_admin_xl_section($sheet, $section, $entrants, $congress, $type);
    }
  }
  // send HTTP headers
  $fileName = 'Entries.xls';
  drupal_add_http_header('Content-Type', 'application/vnd.ms-excel');
  drupal_add_http_header('Content-Disposition', 'attachment; filename="' . $fileName . '"');
  drupal_add_http_header('Cache-Control',  'max-age=0');
  $objWriter = new \PhpOffice\PhpSpreadsheet\Writer\Xls($objPHPExcel);
  $objWriter->save("php://output");
  exit;
}

function congress_admin_xl_section($sheet, $section, $entrants, $congress, $type) {
  // The section header.
  $sheet->SetCellValue('A1', 'ID_No');
  $sheet->SetCellValue('B1', 'Name');
  $sheet->SetCellValue('C1', 'Sex');
  $sheet->SetCellValue('D1', 'Fed');
  $sheet->SetCellValue('E1', 'ClubName');
  $sheet->SetCellValue('F1', 'Birthday');
  $sheet->SetCellValue('G1', 'Rtg_Nat');
  $sheet->SetCellValue('H1', 'Fide_No');
  $sheet->SetCellValue('I1', 'Rtg_Int');
  $sheet->SetCellValue('J1', 'Title');
  $sheet->SetCellValue('K1', 'Bye');
  $sheet->SetCellValue('L1', 'PIN');
  $sheet->SetCellValue('M1', 'Federation');
  $sheet->SetCellValue('N1', 'ClubCode');
  $sheet->SetCellValue('O1', 'EntryID');

  $count=1;
  foreach($entrants as $id => $player)
  {
    $count++;
    // 0 - Players ID, ecfcode or PNUM
    $sheet->SetCellValue("A$count", $player['ecfcode']);
    // 1 - Players Name
    if($type == 'M') {      // Swiss Manager Rating file
      if($congress->country == 'ENG' && isset($player['ecfname']) && strlen($player['ecfname'])>1 ) {
        $name = $player['ecfname'];
      } else {
        $name = $player['last_name'] . ', ' . $player['first_name'];
      }
    } else {
      $name = $player['first_name'] . ' ' . $player['last_name'];
    }
    $sheet->SetCellValue("B$count", $name);
    // 2 - Players gender.
    if($player['sex']== 'M' || $player['sex']== 'F') {
      $sheet->SetCellValue("C$count", $player['sex']);
    }
    // 3 - fed
    $sheet->SetCellValue("D$count", $player['fed']);
    // 4 - club
    $sheet->SetCellValue("E$count", $player['club']);
    // 5 - dob
    $dob = $player['dob'];
    
    // If there is an ECF DOB, use it so can convert a FIDE grade file to ECF.
    // If its a swiss manager rating file and ENG
    if($type == 'M' && $congress->country == 'ENG' ) {
      if(isset($player['ecfdob']) && $player['ecfdob'] != ' ' && strlen($player['ecfdob']) > 2) {
        $dob = $player['ecfdob'];
      }
    }
    if(isset($dob) && $dob != ' ') {
      $dob = new DateTime($dob);
      $dob = \PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel($dob);
      $sheet->SetCellValue("F$count", $dob);
      $sheet->getStyle("F$count")->getNumberFormat()->setFormatCode('dd/mm/yyyy');
    }
    // 6 - rtg_nat
    $sheet->SetCellValue("G$count", $player['nrating']);
    // 7 - fide_no
    if(isset($player['fide_no']) && $player['fide_no'] != 0) {
      $sheet->SetCellValue("H$count", $player['fide_no']);
    }
    // 8 - rtg_int
    $sheet->SetCellValue("I$count", round($player['trating']));
    // 9 - title
    $sheet->SetCellValue("J$count", ' ');
    // 10 - bye
    if(isset($player['bye'])) {
      $sheet->SetCellValue("K$count", $player['bye']);
    }
    // 11 - PIN
    $sheet->SetCellValue("L$count", $count -1);
    // 12 - Federation
    $sheet->SetCellValue("M$count", $player['fed']);
    // 13 - ECF Club Code
    $sheet->SetCellValue("N$count", $player['club']);
    // 14 - Entry ID
    $sheet->SetCellValue("O$count", $id);
  }

  // Set column width
  $sheet->getColumnDimension('A')->setAutoSize(true);
  $sheet->getColumnDimension('B')->setAutoSize(true);
  $sheet->getColumnDimension('C')->setAutoSize(true);
  $sheet->getColumnDimension('D')->setAutoSize(true);
  $sheet->getColumnDimension('E')->setAutoSize(true);
  $sheet->getColumnDimension('F')->setAutoSize(true);
  $sheet->getColumnDimension('G')->setAutoSize(true);
  $sheet->getColumnDimension('H')->setAutoSize(true);
  $sheet->getColumnDimension('I')->setAutoSize(true);
  $sheet->getColumnDimension('J')->setAutoSize(true);
  $sheet->getColumnDimension('K')->setAutoSize(true);
  $sheet->getColumnDimension('L')->setAutoSize(true);
  $sheet->getColumnDimension('M')->setAutoSize(true);
  $sheet->getColumnDimension('N')->setAutoSize(true);
  $sheet->getColumnDimension('O')->setAutoSize(true);
}

function congress_xl_entries_entry($congress,$section,$status) {
  $cid = $congress->cid;
  $sid = $section->sid;
  $rows = array();
  $query = db_select('congress_entry', 'ce');
  $query->condition('ce.cid', $cid);
  $query->fields('ce');
  $query->innerJoin('congress_entry_sections', 'ces', 'ces.eid = ce.eid');
  $query->condition('ces.sid', $sid);
  if($status=='ALL_ENTERED') {
    $query->condition('ce.status', congress_entry_visible_states(), 'IN');
  } else {
    if($status!='ALL') {
      $query->condition('ce.status', $status);
    }
  }
  $query->orderBy('eid', 'ASC');
  $result = $query->execute();

  if ($result) {
    while ($row = $result->fetchAssoc()) {
      if($congress->country == 'ENG') {
        $row['ecfcode'] = ecfdata_add_check_digit($row['ecfcode']);
      }
      $rows[] = $row;
    }
  }
  return $rows;
}

function congress_admin_xl_section_entry($sheet, $section, $entrants) {
  if(count($entrants) == 0) {
    return;
  }

  // Output Headings row
  $headers = array_keys($entrants[0]);
  $col=0;
  foreach($headers as $header) {
    if($header != 'session') {
//    $sheet->SetCellValueByColumnAndRow($col, 1, $header);
      $sheet->SetCellValue([$col+1, 1], $header);
      $col++;
    }
  }

  $row=1;
  foreach($entrants as $entrant )
  {
    $row++;
    $col=0;
    foreach($headers as $header) {
      if($header == 'telephone') {
        $entrant[$header] = ' ' . $entrant[$header];
      }
      if($header == 'dob') {
        if(isset($entrant[$header])) {
          $entrant[$header] = date('Y-m-d',$entrant[$header]);
        }
      }
      if($header == 'updated') {
        $entrant[$header] = date('Y-m-d',$entrant[$header]);
      }
      if($header != 'session') {
//      $sheet->SetCellValueByColumnAndRow($col, $row, $entrant[$header]);
        $sheet->SetCellValue([$col+1, $row], $entrant[$header]);
        $col++;
      }
//    if($header == 'dob') {
//      $sheet->getStyleByColumnAndRow($col, $row)->getNumberFormat()->setFormatCode('dd/mm/yyyy');
//    }
    }
  }

  // Set column width
  $cellIterator = $sheet->getRowIterator()->current()->getCellIterator();
  $cellIterator->setIterateOnlyExistingCells(true);
  /** @var PHPExcel_Cell $cell */
  foreach ($cellIterator as $cell) {
    $sheet->getColumnDimension($cell->getColumn())->setAutoSize(true);
  }
}

function congress_xl_entries($congress,$section,$order='rating',$filter='ALL') {
  $rows = array();
  $players = congress_entrants_query($congress, $section, $filter);

  // Sort by $order
  if( $order == 'rating' ) {
    uasort($players, 'congress_entrants_sort_rating');
  } else {
    ksort($players);
  }
  return $players;
}

function congress_entrants_sort_rating($fa, $fb) {
  $field = 'rating';
  $sign = 1;
  if ($fa[$field] < $fb[$field]) {
      return $sign;
    }
    if ($fa[$field] > $fb[$field]) {
      return -1 * $sign;
    }
    return 0;
}

function congress_admin_txt_types($congress,$type,$filter) {
  if(substr($type,0,1) == 'V') {
    $sid = intval(substr($type,1));
    $suffix = '.CSV';
  } else {
    $sid = intval($type);
    $suffix = '.TXT';
  }
  $section = $congress->sections[$sid];
  $entrants = congress_xl_entries($congress,$section,'id',$filter);
// send HTTP headers
  $fileName = $section->name . $suffix;
  drupal_add_http_header('Content-Type', 'text/plain');
  drupal_add_http_header('Content-Disposition', 'attachment; filename="' . $fileName . '"');
  drupal_add_http_header('Cache-Control',  'max-age=0');
  print "ID no;Name;Sex;Fed;Club;Birth;Rating Nat;FIDE-No;Rating Int;Title;tb1;No;tb2;ClubNo;tb3\n";
  $count=0;
  foreach($entrants as $id => $player)
  {
    $count++;
    // 0 - Players ID, ecfcode or PNUM
    print $player['ecfcode'] . ';';
    // 1 - Players Name
    if($congress->country == 'ENG' && isset($player['ecfname']) && strlen($player['ecfname'])>2 ) {
      $name = $player['ecfname'];
    } else {
      $name = $player['last_name'] . ', ' . $player['first_name'];
    }
    print $name . ';';
    // 2 - Players gender.
    $sex='M';
    if($player['sex']== 'F') {
      $sex='F';
    }
    print $sex . ';';
    // 3 - fed
    print $player['fed'] . ';';
    // 4 - club
    print $player['club'] . ';';
    // 5 - dob
    $dob='';
    if(isset($player['dob']) && $player['dob'] != ' ') {
      $dob = $player['dob'];
    }
    // If there is an ECF DOB, use it so can convert a FIDE grade file to ECF.
    if($congress->country == 'ENG' ) {
      if(isset($player['ecfdob']) && $player['ecfdob'] != ' ' && strlen($player['ecfdob']) > 2) {
        $dob = $player['ecfdob'];
      }
    }
    print $dob . ';';
    // 6 - rtg_nat
    print $player['nrating'] . ';';
    // 7 - fide_no
    $fide_no='';
    if(isset($player['fide_no']) && $player['fide_no'] != 0) {
      $fide_no = $player['fide_no'];
    }
    print $fide_no . ';';
    // 8 - rtg_int
    print $player['trating'] . ';';
    // 9 - title
    print ' ;';
    // ??? tb1
    print '0;';
    // No
    print $count . ';';
    // tb2 - fed again ?
    print $player['fed'] . ';';
    // club no
    print $player['clubId'] . ';';
    // tb3 - ?
    print '1';
    // End of line
    print "\n";
  }
  exit;
}
