<?php
/**
* @file
* Functions for viewing a congress.
*/

function congress_view_page($congress, $view_mode = 'full') {

  $cid = $congress->cid;
  $congress->content = array();

// Build fields content
  field_attach_prepare_view('congress',
                            array($congress->cid => $congress),
                             $view_mode);

  $name = $congress->name;
  congress_content_label_value($congress->content, 'name', 'Name', $name);

  if(is_null($congress->date)) {
    $date = ' ';
  } else {
    $date = date('Y-m-d',$congress->date);
  }
  congress_content_label_value($congress->content, 'date', 'Date', $date);

  // payment
  if(isset($congress->pmid) && $congress->pmid != 0) {
    $payment = entity_load_single('payment_method', $congress->pmid);
    congress_content_label_value($congress->content, 'payment', 'Payment Method', $payment->name);
    $paypal_email = $payment->controller_data['email_address'];
    congress_content_label_value($congress->content, 'ppal_email', 'Payment Email', $paypal_email);
  }

  // organisation
  if(isset($congress->rid) && $congress->rid != 0) {
    $organisation = congress_organisation_load($congress->rid);
    congress_content_label_value($congress->content, 'organisation', 'Organisation', $organisation->name);
  }


  congress_content_label_value($congress->content, 'junior_discount', 'junior_discount', $congress->junior_discount);
  congress_content_label_value($congress->content, 'vetran_discount', 'vetran_discount', $congress->vetran_discount);
  congress_content_label_value($congress->content, 'ask_dob', 'ask_dob', $congress->ask_dob);
  congress_content_label_value($congress->content, 'dob_prompt', 'dob_prompt', $congress->dob_prompt);
  congress_content_label_value($congress->content, 'ask_female', 'ask_female', $congress->ask_female);

  $header = array();
  $header[] = array('data' => 'Name', 'field' => 'name' );
  $header[] = array('data' => 'Rating Type ',   'field' => 'rating_type,' );
  $header[] = array('data' => 'Entry Fee', 'field' => 'entry_fee' );
  $header[] = array('data' => 'Grading Limit', 'field' => 'grade_limit' );
  $header[] = array('data' => 'Rate', 'field' => 'speed' );
  $header[] = array('data' => 'Rounds', 'field' => 'rounds' );
  $header[] = array('data' => 'Byes', 'field' => 'byes' );

  $speeds = congress_speed_options();
  $rows = array();
  foreach($congress->sections as $section) {
    $speed = $speeds[$section->speed];
    $newrow = array( $section->name,
                     $section->rating_type,
                     $section->entry_fee,
                     $section->grade_limit,
                     $speed,
                     $section->rounds,
                     $section->byes );
    $rows[] = $newrow;
  }
  $congress->content['sections']['#rows'] = $rows;
  $congress->content['sections']['#header'] = $header;
  $congress->content['sections']['#theme'] = 'table';


  $congress->content += field_attach_view('congress', $congress, $view_mode);
  return $congress->content;
}
