<?php
/**
* @file
* Functions for uploading files for a congress organisation.
*/

function congress_file_upload_page($rid) {

  $content = array();
  $organisation = congress_organisation_load($rid);

  $title = "Files for: $organisation->name";
  $content['title'] = array(
     '#type' => 'markup',
     '#markup' => "<h3>$title</h3>",
    );

  $content['form'] = drupal_get_form('congress_file_upload_form', $organisation);

  $table = congress_uploaded_files($rid);
  $rows = $table['data'];     // Actual table data
  $header = $table['header']; // Headers

  $content['table']['#rows'] = $rows;
  $content['table']['#header'] = $header;
  $content['table']['#theme'] = 'table';

  return $content;
}

function congress_uploaded_files($rid) {

  $header = array();
  $header[] = array('data' => 'Name', 'field' => 'name', 'sort'=>'asc' );
  $header[] = array('data' => 'Delete', 'field' => 'delete' );

  $rows = array();
  $path = "public://congress/$rid/*";
  $realpath = drupal_realpath($path);
  $files = glob($realpath);
  foreach($files as $file) {
    $local_file = basename($file);
    // TODO link to the file
    $file_url = file_create_url("public://congress/$rid/$local_file");
    $file_link = l($local_file, $file_url);
    $delete = l('delete', "congress/delfile/$rid/$local_file");
    $row = array($file_link, $delete);
    $rows[] = $row;
  }

  $table['data'] = $rows;
  $table['header'] = $header;
  return $table;
}
 
/**
*  Form for uploading a file.
*/
function congress_file_upload_form($form, &$form_state, $organisation) {

  $form_state['org'] = $organisation;
  $rid = $organisation->rid;

  // Required to do file uploads
  $form['#attributes'] = array('enctype' => "multipart/form-data");

  // File upload field
  $form['file'] = array(
    '#type' => 'file',
    '#title' => t('File to Upload'),
    '#description' => t('Name of file to upload'),
    '#size' => 64,
  );

  // button.
  $form['submit'] = array(
     '#value' => t('Load File'),
     '#type'  => 'submit',
  );

  return $form;
}

/**
*  Action after the form is submitted.
*/
function congress_file_upload_form_submit($form, &$form_state) {
  $organisation = $form_state['org'];
  $rid = $organisation->rid;

  $validators = array(
   'file_validate_extensions' => array('xls XLS csv CSV doc DOC docx DOCX pdf PDF'),
   'file_validate_size' => array(2000000, 0),
  );

  // Create module subdirectory if it doesn't exist
  $path = 'public://congress';
  if (!file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {
    drupal_set_message(t('Could not create file @file', array('@file' => $path)), 'error');
    return;
  }

  // Create uploaded files subdirectory if it doesn't exist
  $path .= "/$rid";
  if (!file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {
    drupal_set_message(t('Could not create file @file', array('@file' => $path)), 'error');
    return;
  }

  // Upload the file
  if ($xlsfile = file_save_upload('file', $validators, $path, FILE_EXISTS_REPLACE)) {
    $xlsfile->status = FILE_STATUS_PERMANENT;
    file_save($xlsfile);
    $filename = $xlsfile->destination;
    drupal_set_message(t('Uploaded @file', array('@file' => $filename)));
  }
}

function congress_file_delete($rid, $file) {
  $path = "public://congress/$rid/$file";
  $realpath = drupal_realpath($path);
  unlink($realpath);
  drupal_set_message(t('File @file deleted', array('@file' => $file)));
  drupal_goto("congress/admin/$rid/files");
}
