<?php
/**
 * @file
 * Congress Entry controller
 */

class CongressEntryController extends DrupalDefaultEntityController {
  public function create($cid) {
    return (object) array(
      'eid' => '',
      'cid' => $cid,
      'oid' => 0,
      'first_name' => '',
      'last_name' => '',
      'email' => '',
      'address1' => '',
      'address2' => '',
      'city' => '',
      'postcode' => '',
      'telephone' => '',
      'donation' => '',
      'silver' => 'N',
      'late' => '0',
      'price' => '0',
      'sex' => ' ',
      'dob' => null,
      'ecfcode' => '',
      'ecfmemb' => '',
      'fidecode' => '',
      'federation' => 'XXX',
      'grade' => '',
      'grade_details' => '',
      'club' => '',
      'extra' => '0',
      'status' => 'CREATED',
      'updated' => null,
      'age_option' => 'N',
      'onlineid' => '',
      'extra_reply' => '',
      'session' => '',
      'sections' => array(),
    );
  }

  public function section($eid,$sid) {
    return (object) array(
      'eid' => $eid,
      'sid' => $sid,
      'bye1' => 0,
      'bye2' => 0,
    );
  }

  public function load($ids = array(), $conditions = array()) {
    $objects = parent::load($ids, $conditions);
    foreach($objects as $object) {
      $object->sections = $this->load_sections($object->eid);
    }

    return $objects;
  }

  private function save_sections($entry) {

    // Delete any existing sections
    db_delete('congress_entry_sections')
          ->condition('eid', $entry->eid)
          ->execute();

    // Add sections
    foreach($entry->sections as $section) {
      $section->eid = $entry->eid;
      drupal_write_record('congress_entry_sections', $section);
    }
  }

  private function load_sections($eid) {
    $sections = array();
    $query = db_select('congress_entry_sections', 'cs');
    $query->condition('cs.eid', $eid);
    $query->fields('cs', array('eid', 'sid', 'bye1', 'bye2' ));
    //print out query
    //dsm((string)$query);
    $result = $query->execute();
    // Transfer from object $row to array rows
    foreach ( $result as $row) {
      $sections[$row->sid] = (object)$row;
    }

    return $sections;
  }


  public function save($entry) {
    $entry->updated = time();
    $transaction = db_transaction();

    try {
      $entry->is_new = empty($entry->eid);
      if($entry->is_new) {
        drupal_write_record('congress_entry', $entry);
        $op = 'insert';
      } else {
        drupal_write_record('congress_entry', $entry, 'eid');
        $op = 'update';
      }
      // Save fields
      $function = 'field_attach_' . $op;
      $function('congress_entry', $entry);

      module_invoke_all('entity_' . $op, $entry, 'congresss_entry');
      unset($entry->is_new);

      // Save sections
      $this->save_sections($entry);

      db_ignore_slave();

      return $entry;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('congress', $e, NULL, WATCHDOG_ERROR);
      return FALSE;
    }
  }

  public function delete($eids) {
    if(!empty($eids)) {
      $entries = $this->load($eids, array());
      $transaction = db_transaction();

      try {
        db_delete('congress_entry')
          ->condition('eid', $eids, 'IN')
          ->execute();

        foreach($entries as $eid => $entry) {
          field_attach_delete('congress_entry', $entry);
        }

        db_ignore_slave();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('congress', $e, NULL, WATCHDOG_ERROR);
        return FALSE;
      }

      module_invoke_all('entity_delete', $entry, 'congress_entry');

      cache_clear_all();
      $this->resetCache();
    }
    
    return TRUE;
  }
}

